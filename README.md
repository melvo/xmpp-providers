<!--
SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# [XMPP Providers](https://providers.xmpp.net) - Curated List of Providers for Registration and Autocomplete

[![XMPP Providers: A](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/count-badge-A.svg?job=badges)](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/providers-A.json?job=filtered-provider-lists)
[![XMPP Providers: B](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/count-badge-B.svg?job=badges)](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/providers-B.json?job=filtered-provider-lists)
[![XMPP Providers: C](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/count-badge-C.svg?job=badges)](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/providers-C.json?job=filtered-provider-lists)
[![XMPP Providers: D](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/count-badge-D.svg?job=badges)](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/providers-D.json?job=filtered-provider-lists)

[![REUSE status](https://api.reuse.software/badge/invent.kde.org/melvo/xmpp-providers)](https://api.reuse.software/info/invent.kde.org/melvo/xmpp-providers)

[TOC]

## Introduction

This project provides a machine-readable curated list of [XMPP](https://xmpp.org/about/technology-overview/) providers and a tool for filtering it.
The JSON list can be used by XMPP clients or other services for provider suggestions.
Visit the [official website](https://providers.xmpp.net) to see it in action!

Each **client** has different requirements on a provider: The tool in this repository can be used to create **statically filtered** provider lists.

Each **user** has different requirements on a provider: At runtime, the statically filtered lists can be **dynamically filtered** on the user's demands.

## Main Goals

This project has two main goals:

1. Simplifying the onboarding of new XMPP users by a list of XMPP providers that can be integrated into XMPP clients and used for choosing a provider for registration
1. Improving the providers' features, security, support and documentation by defining high quality standards and providing information to achieve them

## Contributing

Help us to make *free communication* possible and contribute to this project!
You can also help us with the [website](https://github.com/xsf/xmpp-providers-website), the [download service](https://github.com/xsf/infrastructure/tree/master/xmpp-providers-docker) or the [server setup](https://invent.kde.org/melvo/xmpp-providers-server).

**Please read the [contribution guidelines](/CONTRIBUTING.md) carefully before creating a merge request.**
That saves you and the reviewers a lot of time during the review.

## Categories

The providers can be manually filtered to match specific requirements.
However, to provide a standardized way of categorizing providers, there are pre-defined criteria for filtering.

The following categories are used to distinguish between different quality levels the checked provders can offer.
Category A and B offer the best, C an average and D the worst user experience during onboarding and further usage.
Therefore, **A and B are the recommended categories for registrations**!
Category D contains all providers.

Category A is a subset of B, B a subset of C and C a subset of D.
Thus, providers of A can be used for the purposes of B, C and D as well.
Providers of B can be used for the purposes of C and D as well.
Providers of C can be used for the purposes of D as well.

### Category A: Automatically Chosen

Providers in this category can be used for an automatic registration.

### Category B: Manually Selectable

Providers in this category can be used for a manual registration.

### Category C: Completely Customizable

Providers in this category can be used for completely customized filtering.

### Category D: Autocomplete

Providers in this category can be used for autocomplete.

## Usage

If you are an XMPP client developer or would like to create a service based on XMPP Providers, you can create filtered lists on your own or use the daily-updated prefiltered lists.

The prefiltered lists are versioned and available for all categories (replace `<version>` and `<category>` with the desired values):

```plaintext
https://data.xmpp.net/providers/v<version>/providers-<category>.json
```

Example for a prefiltered list with providers of category **B**:

```plaintext
https://data.xmpp.net/providers/v2/providers-B.json
```

Prefiltered **s**imple lists, which contain only the provider domains, are also available:

```plaintext
https://data.xmpp.net/providers/v<version>/providers-<category>s.json
```

Example for a prefiltered simple list with providers of category **D**:

```plaintext
https://data.xmpp.net/providers/v2/providers-Ds.json
```

The original (i.e., unfiltered) providers file including all sources and comments can be downloaded as well but should only be used by services that need the additional information:

```plaintext
https://data.xmpp.net/providers/v<version>/providers.json
```

Example for the providers file:

```plaintext
https://data.xmpp.net/providers/v2/providers.json
```

### Versioning

The version is a natural number.
It is incremented when a new property is added, an existing property renamed or deleted.
Data of old versions is updated for at least six months.
Afterwards, the latest state of the data is provided for additional 2 years.

### Client Integration

Here are the recommended steps to integrate the provider lists in an XMPP client:

1. Download a prefiltered [provider list for category B](https://data.xmpp.net/providers/v2/providers-B.json).
1. Download a prefiltered [simple provider list for category D](https://data.xmpp.net/providers/v2/providers-Ds.json).
1. Include both lists in your client's source code / executable.
1. If you have a registration process without user interaction (i.e., automatic registration such as [Kaidan's quick onboarding](https://www.kaidan.im/2020/01/08/Easy-Registration/)), retrieve the providers of category A from the list for category B.
1. For a manual registration, use all providers from the list of category B.
1. Use the list for category D to autocomplete chat addresses after registration.

The provider lists are primarily used to choose a provider that suits the user best.
Its data can be displayed or used to filter suitable providers during the manual registration.
But the provider list for category B can also be used during or after registration for specific actions (e.g., to open a provider's website or send a support message).

You should also create a script or use another way to update the integrated provider lists on a regular basis.

## Properties

The **providers file** `providers.json` is the source to create all filtered **provider lists**.
All properties in this section are in the providers file *and* in the provider lists.
The additional property `category` is only in the provider lists.
It indicates a provider's category as an upper-case letter.

A provider's JID (e.g., `example.org`) is mapped to the provider's properties.
Each provider is added with its main JID (i.e., the JID that resembles the website's domain the most or is promoted first).
In the providers file, the JID is the key of the corresponding provider's JSON object.
Whereas, in the provider lists, the JID is a JSON object within the JSON array's value for the corresponding provider.

### Provider File

A provider file is a JSON file containing only the provider properties that cannot be retrieved via other methods.
It is used to automatically update the providers file.
Each provider can [generate a provider file](https://providers.xmpp.net/provider-file-generator/) and supply it via its web server (replace `<provider>` and `<version>` with the desired values):

```plaintext
https://<provider>/.well-known/xmpp-provider-v<version>.json
```

Example for a provider file of the latest version **2**:

```plaintext
https://example.org/.well-known/xmpp-provider-v2.json
```

### Basic Information

The following properties do **not affect** the provider's **category**:

Information (Key in JSON File) | Description / Data Type / Unit of Measurement | [Automatic Updates](TOOLS.md#automation) | Since | Changes
---|---|---|---|---
`latestChange` | Date of the latest change formatted as [YYYY-MM-DD](https://en.wikipedia.org/wiki/ISO_8601#Dates) (e.g., `2021-01-16`) | Updated on each change | `v2` | Replaced `lastCheck`
`website` | Mappings from lower-case [two-letter language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to [URL](https://en.wikipedia.org/wiki/URL) of website language variant or `{}` for n/a | [Provider file](#provider-file) | `v1` | -
`alternativeJids` | List of JIDs (XMPP server domains) a provider offers for registration other than its main JID or `[]` for n/a | [Provider file](#provider-file) | `v2` | -
`busFactor` | [Bus factor](https://en.wikipedia.org/wiki/Bus_factor) of the XMPP service (i.e., the minimum number of team members that the service could not survive losing or `-1` for n/a | [Provider file](#provider-file) | `v1` | -
`organization` | Type of organization providing the XMPP service (one of `company`, `commercial person`, `private person`, `governmental`, `non-governmental` or `""` for n/a) | [Provider file](#provider-file) | `v2` | Replaced `company`
`passwordReset` | Mappings from lower-case [two-letter language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to [URL](https://en.wikipedia.org/wiki/URL) of web page language variant used for automatic password reset (e.g., via email) / describing how to manually reset password (e.g., by contacting the provider) or `{}` for n/a | [Provider file](#provider-file) | `v1` | -
`serverTesting` | Whether tests against the provider's server are allowed (e.g., certificate checks and uptime monitoring) (`true` or `false`) | [Provider file](#provider-file) | `v2` | -
`serverSoftwareName` | Name of the XMPP server software or `""` for n/a | [XEP-0092: Software Version](https://xmpp.org/extensions/xep-0092.html) | `v1` | -
`serverSoftwareVersion` | Version of the XMPP server software or `""` for n/a | [XEP-0092: Software Version](https://xmpp.org/extensions/xep-0092.html) | `v1` | -
`ratingGreenWebCheck` |  Whether the XMPP server is hosted eco-friendly (`true` or `false`) | [Green Web Check](https://www.thegreenwebfoundation.org/green-web-check/) | `v1` | -
`inBandRegistrationEmailAddressRequired` | Whether an email address must be submitted for account creation via XMPP clients (`true` or `false`) | [XEP-0077: In-Band Registration](https://xmpp.org/extensions/xep-0077.html#usecases-register) | `v2` | -
`inBandRegistrationCaptchaRequired` | Whether a [CAPTCHA](https://en.wikipedia.org/wiki/Captcha) must be solved for account creation via XMPP clients (`true` or `false`) | [XEP-0077: In-Band Registration](https://xmpp.org/extensions/xep-0077.html#usecases-register) | `v2` | -

### Criteria

The table shows the following two circumstances for category A and B:

A provider is in a specific category if it meets all of the criteria listed in the table below.
A condition can be `true`, `false` or true if a specific case such as greater or lower than a specific value is applicable.
If the data type is a list or mapping, a condition including numbers corresponds to the count of their elements.
If the data type is a date, a condition including numbers corresponds to the age in days.

Here is an example for the in-band and web registration:
*A provider that has no inBandRegistration is not in category A.*
*But if the provider has a registrationWebPage, it is in category B if it also meets all of the other criteria for B.*

The following properties **affect** the provider's **category**:

Criterion (Key in JSON File) | Description / Data Type / Unit of Measurement | [Automatic Updates](TOOLS.md#automation) | Since | Changes | Category A | Category B | Category C
---|---|---|---|---|---|---|---
`inBandRegistration` | Whether account creation via XMPP clients is supported (`true` or `false`) | [XEP-0077: In-Band Registration](https://xmpp.org/extensions/xep-0077.html#usecases-register) | `v1` | - | `true` | `true or registrationWebPage >= 1` | `true or registrationWebPage >= 1`
`registrationWebPage` | Mappings from lower-case [two-letter language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to [URL](https://en.wikipedia.org/wiki/URL) of web page language variant for account creation or `{}` for n/a | [XEP-0077: In-Band Registration](https://xmpp.org/extensions/xep-0077.html#redirect) | `v1` | - | `>= 1 or inBandRegistration` | `>= 1 or inBandRegistration` | `>= 1 or inBandRegistration`
`ratingXmppComplianceTester` | To what extend the XMPP server supports features used by most modern XMPP clients (number in percentage or `-1` for n/a) | [XMPP Compliance Tester](https://compliance.conversations.im) | `v1` | - | `= 100` | `= 100` | `>= 90`
`ratingImObservatoryClientToServer` | To what extent the XMPP server supports secure connections to XMPP clients (upper-case letter) | Not automatically updated since [IM Observatory](https://xmpp.net) is unmaintained | `v1` | - | `>= A` | `>= A` | `>= A`
`ratingImObservatoryServerToServer` | To what extent the XMPP server supports secure connections to XMPP servers (upper-case letter) | Not automatically updated since [IM Observatory](https://xmpp.net) is unmaintained | `v1` | - | `>= A` | `>= A` | `>= A`
`maximumHttpFileUploadFileSize` | Maximum size of each shared file (number in megabytes (MB), `0` for no limit or `-1` for n/a or less than 1 MB) | [XEP-0363: HTTP File Upload](https://xmpp.org/extensions/xep-0363.html#disco) | `v1` | - | `0 or >= 20` | `0 or >= 20` | `>= 0`
`maximumHttpFileUploadTotalSize` | Maximum size of all shared files in total per user (number in megabytes (MB), `0` for no limit or `-1` for n/a or less than 1 MB) | [Provider file](#provider-file) ([XEP-0363: HTTP File Upload](https://xmpp.org/extensions/xep-0363.html) does not provide that information) | `v1` | -  | `0 or >= 100` | `0 or >= 100` | `>= 0`
`maximumHttpFileUploadStorageTime` | Maximum storage duration of each shared file (number in days, `0` for no limit or `-1` for n/a or less than 1 day) | [Provider file](#provider-file) ([XEP-0363: HTTP File Upload](https://xmpp.org/extensions/xep-0363.html) does not provide that information) | `v1` | - | `0 or >= 7` | `0 or >= 7` | `>= 0`
`maximumMessageArchiveManagementStorageTime` | Maximum storage duration of each exchanged message (number in days, `0` for no limit or `-1` for n/a or less than 1 day) | [Provider file](#provider-file) ([XEP-0313: Message Archive Management](https://xmpp.org/extensions/xep-0313.html) does not provide that information) | `v1` | - | `0 or >= 7` | `0 or >= 7` | `>= 0`
`professionalHosting` | Whether the XMPP server is hosted with good internet connection speed, uninterruptible power supply, access protection and regular backups (`true` or `false`) | [Provider file](#provider-file) | `v1` | - | `true` | `true` | `true or false`
`freeOfCharge` | Whether the XMPP service can be used for free (`true` or `false`) | [Provider file](#provider-file) | `v1` | - | `true` | `true or false` | `true or false`
`legalNotice` | Mappings from lower-case [two-letter language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to [URL](https://en.wikipedia.org/wiki/URL) of legal notice language variant or `{}` for n/a | [Provider file](#provider-file) | `v1` | - | `>= 1` | `>= 1` | `>= 0`
`serverLocations` | List of lower-case [two-letter country codes](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) for server/backup locations or `[]` for n/a | [Provider file](#provider-file) | `v1` | - | `>= 1` | `>= 1` | `>= 0`
`emailSupport` | Mappings from lower-case [two-letter language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to list of email addresses for user support or `{}` for n/a | [XEP-0157: Contact Addresses for XMPP Services](https://xmpp.org/extensions/xep-0157.html) | `v1` | - | `>= 1 or chatSupport >= 1 or groupChatSupport >= 1` | `>= 1 or  or chatSupport >= 1 or groupChatSupport >= 1` | `>= 1 or  or chatSupport >= 1 or groupChatSupport >= 1`
`chatSupport` | mappings from lower-case [two-letter language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to list of chat addresses for user support or `{}` for n/a | [XEP-0157: Contact Addresses for XMPP Services](https://xmpp.org/extensions/xep-0157.html) | `v1` | - | `>= 1 or emailSupport >= 1 or groupChatSupport >= 1` | `>= 1 or emailSupport >= 1 or groupChatSupport >= 1` | `>= 1 or emailSupport >= 1 or groupChatSupport >= 1`
`groupChatSupport` | mappings from lower-case [two-letter language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to list of group chat addresses for user support or `{}` for n/a | [XEP-0157: Contact Addresses for XMPP Services](https://xmpp.org/extensions/xep-0157.html) | `v1` | - | `>= 1 or emailSupport >= 1 or chatSupport >= 1` | `>= 1 or emailSupport >= 1 or chatSupport >= 1` | `>= 1 or emailSupport >= 1 or chatSupport >= 1`
`since` | Date since the XMPP service is available or listed for n/a formatted as [YYYY-MM-DD](https://en.wikipedia.org/wiki/ISO_8601#Dates) (e.g., `2021-01-16`) | [Provider file](#provider-file) | `v1` | - | `> 365` | `> 365` | `>= 0`

## Tools

There are [tools](/TOOLS.md) for automatically querying provider properties, generating filtered provider lists and other tasks.

## Artifacts

GitLab CI job [artifacts](/ARTIFACTS.md) are provided to be used by external services.
