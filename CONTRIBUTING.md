<!--
SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Contributing

[TOC]

## Introduction

**ATTENTION: Please do not create merge requests with manually modified providers** (except project members) since many procedures within this project are automated!
[Open issues for providers](#providers) instead.

If you are familiar with Git, GitLab, Python and XMPP, you can become a project member too.
Please get in touch with us!

There are mainly the following ways to contribute:

1. [Adding, renaming or removing **providers**](#providers)
1. [Adding, moving, renaming, removing **properties**](#properties)
1. [Developing **tools**](#development) processing the providers, badges and the tools themselves
1. Updating **providers** (i.e., changing their property values) by adding methods to the corresponding [automation tools](TOOLS.md#automation) or (in corner cases) by manual modifications

The [providers file](providers.json) is the main resource of this project.
It contains entries for providers and their properties.
Providers are semi-automatically added, renamed or removed after an initial manual request.
Their property values are updated automatically, though.

The [automatic updates](https://invent.kde.org/melvo/xmpp-providers/-/pipeline_schedules) are run daily.
Checking and updating the latest properties is done by automation tools.
But in rare cases, manual updates may be needed.
In addition, there are other important tools such as one for checking URLs or one for creating provider badges.

All those tools need to be improved and extended.
In order to achieve that, we welcome everybody to contribute to this project.
Feel free to [contact us](https://providers.xmpp.net/contact/)!

## Providers

This section should help contributors with adding, renaming and removing providers.

Please [open an issue](https://invent.kde.org/melvo/xmpp-providers/-/issues) for each provider that should be added, renamed or removed.
The issue's title should be of the form `providers: Add example.org`, `providers: Rename example.org to example.com` or `providers: Remove example.org`.
After opening the issue, a project member creates a merge request to address the issue.
Please reply to any questions.

**Do not open a merge request on your own for provider modifications if you are not a project member!**
Most of the process is automated or at least assisted.
Thus, only project members, which are aware of the process, should open merge requests.

### Provider Modification Conditions

This section describes which conditions must be met in order to modify the providers.

#### Conditions for Adding Providers

A provider can be **added** if all of the following aspects are fulfilled:

* The provider is public (i.e., it provides its service to the public even with currently closed/restricted registrations).
* The provider does not (obviously and actively) behave against the human rights.

#### Conditions for Renaming Providers

A provider can be **renamed** in the following cases:

* The provider has changed its name/domain/JID.

#### Conditions for Removing Providers

A provider can be **removed** in the following cases:

* The provider does not exist anymore.
* The provider does not fulfill the mentioned aspects anymore.

A provider that has been removed because of the mentioned aspects can be added again once they are fulfilled.

## Properties

This section should help contributors with adding, moving, renaming and removing properties.

All providers have a common set of [properties](README.md#properties).
Please [open an issue](https://invent.kde.org/melvo/xmpp-providers/-/issues) for each property that should be added, moved, renamed or removed.
The issue's title should be of the form `Add property 'busFactor'`, `Move property 'passwordReset'`, `Rename property 'lastCheck' to 'latestChange'` or `Remove property 'legalNotice'`.
After opening the issue, a project member creates a merge request to address the issue.
Please reply to any questions.

**Do not open a merge request on your own for property modifications if you are not a project member!**
Most of the process is automated or at least assisted.
Thus, only project members, which are aware of the process, should open merge requests.

### Property Modification Conditions

This section describes which conditions must be met in order to modify the provider properties.

#### Conditions for Adding Properties

A property can be **added** if all of the following aspects are fulfilled:

* The property is relevant to users of this project.
* The property is objective.
* The property has a suitable name.
* The property has a machine-readable value.
* The property has a source or will have a source soon.

#### Conditions for Moving Properties

A property can be **moved** in the following cases:

* The property is better suited at another place.

#### Conditions for Renaming Properties

A property can be **renamed** in the following cases:

* The property 's name does not fit its meaning.

#### Conditions for Removing Properties

A property can be **removed** in the following cases:

* The property is not relevant to users of this project anymore.
* The property cannot be retrieved from its source anymore.

A property that has been removed because of the mentioned aspects can be added again once they are fulfilled.

## Maintenance

**This section is only for project members!**
Please read the [README](README.md) and stick to the following rules.

### Maintaining the Providers

This section should help project members to maintain the providers file.
Run the [provider manager](TOOLS.md#provider-manager) for provider modifications.
Manual modifications should only be needed in rare cases or for files mentioned by the property manager on success.

#### For New Providers

1. Check whether the provider meets the [conditions for adding providers](#conditions-for-adding-providers).
    * If the conditions are not met, provide that information to the corresponding issue (if existent).
1. Run the [provider manager](TOOLS.md#provider-manager) in order to add the provider.
    * If a provider offers **multiple domains** usable for XMPP (i.e., JIDs), add only the domain that corresponds to the domain of the **provider's website**.
    * In case the domain of the website differs from all offered JIDs, use the provider's **main JID** (i.e., the JID that resembles the website's domain the most or is promoted first).
1. Send a **test request** to each available support address
    * If you do not receive a **response within 7 days**, insert `"content": []` and `"comment": "admin@example.org: No response"` (`admin@example.org` must be replaced by the actual address).

#### For All Providers

* **Sort** the entries in the commit message in **alphabetically ascending** order by their JIDs.
* As soon as you made all changes to the provider list, add all changes via `git add .` and commit them via `git commit`.
* A Git pre-commit hook is automatically run for identifying **syntax errors** and applying a consistent **format**.
* Use **different commits** for adding, updating, renaming and removing entries.
* If you add, update or remove entries, use the following **commit message** structure: `providers: Add/Update/Rename/Remove <added/updated/renamed/removed providers>` where `<added/updated/renamed/removed providers>` is replaced by the addresses of the providers or by `all providers`, each separated by a comma.
* If there is a corresonding issue for the changes:
  * Add the label *Providers* to the issue.
  * Link to the issue within the commit message after an empty line via `Closes #<issue number>` in order to automatically close the issue once the corresponding commit is merged.
* Create a merge request for each commit or provide us the information via another channel (chat, email, online resource).

#### Provider Commit Message Examples

```shell
providers: Add example.org

Closes #100
```

* `providers: Add example.com, example.org`
* `providers: Update example.com, example.net`
* `providers: Update all providers`
* `providers: Rename example.org to example.com, example.net to example.org`
* `providers: Remove example.org`

### Maintaining the Properties

This section should help project members to maintain the files that process properties.
Run the [property manager](TOOLS.md#property-manager) for property modifications.
Manual modifications should only be needed for files mentioned by the property manager on success.

* As soon as you made all changes, add them via `git add .` and commit them via `git commit`.
* Use **different commits** for adding, moving, renaming and removing properties.
* If you add, move, rename or remove a property, use the following **commit message** structure: `Add/Move/Rename/Remove '<added/moved/renamed/removed property>'` where `<added/moved/renamed/removed property>` is replaced by the property name.
* If there is a corresonding issue for the changes:
  * Add the label *Properties* to the issue.
  * Link to the issue within the commit message after an empty line via `Closes #<issue number>` in order to automatically close the issue once the corresponding commit is merged.

#### Property Commit Message Examples

```shell
Add property 'busFactor'

Closes #100
```

* `Add property 'busFactor'`
* `Move property 'passwordReset'`
* `Rename property 'lastCheck' to 'latestChange'`
* `Remove property 'legalNotice'`

## Development

This section is only for contributors improving the Python scripts (called **tools**).
If you want to modify providers or properties, have a look at the previous sections.

**You should be familiar with Git, GitLab, Python and XMPP for contributing to this project.**
Please create branches on your forked repository and submit merge requests for them.
Do not create branches on XMPP Providers' main repository.
Do not push your commits directly to its master branch.

### Setup

Please follow the steps explained in [Kaidan's basic contribution setup](https://invent.kde.org/network/kaidan/-/wikis/setup) if you are unfamiliar with KDE Identity, GitLab or Git.
It should answer most of your questions.
You mostly have to replace `network/kaidan` by `melvo/xmpp-providers` in the text where necessary.

Set up everything needed to work inside of your local repository:

```shell
cd xmpp-providers
./setup.sh
```

### Merge Requests (MR)

Currently, Melvin Keskin (@melvo), Edward Maurer (@echolonone) and Daniel Brötzmann (@wurstsalat) are the maintainers of XMPP Providers.
They are responsible for accepting MRs.
Nevertheless, all experienced developers can open MRs.
All project members can review MRs and give feedback on them.

Please stick to the following steps for opening, reviewing and accepting MRs.

#### For Authors

1. Create a new branch to work on it from the [master branch](https://invent.kde.org/melvo/xmpp-providers/-/tree/master).
1. Write short commit messages starting with an upper case letter and the imperative.
1. If your commit touches only one file, start the commit message with the filename (without the extension) as in `README: Add section 'Categories'` or `criteria: Require 20 MB as 'maximumHttpFileUploadFileSize' for category A`.
1. If your commit closes an issue, add the line `Closes #<commit-number>` (Example: `Closes #100`) to the commit message's end after a separating empty line.
1. Split your commits logically.
1. Do not mix unrelated changes in the same MR.
1. Create an MR with the *master* branch as its target.
1. Add `Draft: ` in front of the MR's title as long as you are working on the MR and remove it as soon as it is ready to be reviewed. <!-- markdownlint-disable-line MD038 -->
1. If an MR needs another MR to work, add the line `Depends on !<mr-number>` (Example: `Depends on !100`) to the MR description's end after a separating empty line.
1. A maintainer and possibly other project members will give you feedback.
1. Improve your MR according to their feedback, push your commits and close open threads via the *Resolve thread* button.
1. If necessary, modify, reorder or squash your commits, rebase them on the master branch and force-push (`git push -f`) the result to the MR's branch.
1. As soon as all threads on your MR are resolved, a maintainer will merge your commits into the *master* branch.

Please do not merge your commits into the *master* branch on your own.
If maintainers approved your MR but have not yet merged it, that probably means that they are waiting for the approval of additional maintainers.
Feel free to ask if anything is unclear.

#### For Reviewers

1. Provide detailed descriptions of found issues to the author.
1. Try to give the author concrete proposals for improving the code via the *Insert suggestion* button while commenting.
1. If the proposals are too complicated, create and push a commit with your proposal to your own fork and open an MR with the author's MR branch as its target.
1. In case you are a maintainer:
    1. If you think that no additional review is needed, make editorial changes (such as squashing the commits) and merge the result directly.
    1. If you would like to get (more) feedback from other maintainers, approve the MR using the *Approve* button and mention other maintainers to review the MR.
1. In case you are not a maintainer but a project members and you think that the MR is ready to be merged, approve the MR using the *Approve* button.

Reviews should be done by at least one maintainer not involved as the MR's author or co-author.
