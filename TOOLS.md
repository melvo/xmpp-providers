<!--
SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Tools

[TOC]

## Introduction

There are scripts for various tasks such as updating or filtering the providers.
Most of those tools can be run via the [base tool](#base-tool).

## Base Tool

The base tool `tools/run.py` is used to execute the available tools.
It runs one tool at a time.
You can pass top-level options (called **base tool options**) which are applied to all executed tools.

Run the following command to get a list of possible arguments:

```shell
python3 -m tools.run -h
```

Help for individual tools is available by placing the option `-h` after the tool command.

Example to get help for the URL checker run via the command `check_urls`:

```shell
python3 -m tools.run check_urls -h
```

For debugging, run the base tool with the option `-d`.

Example to run the URL checker in debug mode:

```shell
python3 -m tools.run -d check_urls
```

## Provider Manager

The tool `tools/provider_manager.py` can be used to manage the providers.
It modifies the providers in `providers.json`.

### Provider Manager Usage

An interactive dialog opens when you run the tool:

```shell
python3 -m tools.run provider_manager
```

## Property Manager

The tool `tools/property_manager.py` can be used to manage the properties providers have.
It modifies the property definitions in `properties.json`.

Additonally, it adds new properties to the providers file or removes existing properties from it.
However, it does neither change the criteria for filtering providers in `tools/criteria.py` nor their documentation in `README.md`.
Thus, you need to do that manually if needed.

### Property Manager Usage

An interactive dialog opens when you run the tool:

```shell
python3 -m tools.run property_manager
```

## Prettify Tool

The tool `tools/prettify.py` validates providers/clients JSON files and applies a consistent format to them.
For providers files, the properties and their order are defined in `provider-properties.json`.
For clients files, `client-properties.json` is used.

### Prettify Tool Usage

All suitable known JSON files are validated and formatted if you run the tool without arguments:

```shell
python3 -m tools.run prettify
```

The tool can quit with **e**xit codes enabling other tools to determine whether user interaction is needed before continuing with further steps.

```shell
python3 -m tools.run prettify -e
```

To process only specific JSON files or those that are unknown to the tool, pass them along with their type specifier to the command.

Example for processing the unknown **p**roviders files `providers-1.json`, `providers-2.json` and the unknown **c**lients files `clients-1.json`, `clients-2.json`:

```shell
python3 -m tools.run prettify -p providers-1.json providers-2.json -c clients-1.json clients-2.json
```

## Filter Tool

The tool `tools/filter.py` can be used to create filtered lists of providers (called **provider lists**).

### Filter Tool Usage

Provider lists for all categories are created if you run the tool without arguments:

```shell
python3 -m tools.run filter
```

If you want to create a filtered list which can be used by a client, simply enter the category name.

Example for creating a list of category **A** providers:

```shell
python3 -m tools.run filter -A
```

You can create a provider list containing all providers for completely customized filtering (e.g., by an own filter tool or at runtime):

```shell
python3 -m tools.run filter -C
```

A **s**imple list containing only the domains of the providers is also possible.

Example for creating a domain list of category **D** providers to use it only for autocomplete:

```shell
python3 -m tools.run filter -s -C
```

The **c**ategories of the filtered providers can be included in their entries.

Example for creating a list of category **D** providers that includes their best categories.

```shell
python3 -m tools.run filter -c -D
```

You can create files containing the **r**esults of the filtering (called **categorization results**).
Those files include the providers' properties that do not meet the criteria for being in specific categories.

Example for creating result files for all providers:

```shell
python3 -m tools.run filter -r
```

If you are interested in specific providers, you can append them to the command.

Example for creating a list of category **B** providers out of `example.org` and `example.com`:

```shell
python3 -m tools.run filter -B example.org example.com
```

The tool can be run in **d**ebug mode to see why providers are not in a specific category.

Example for creating a list of category **A** providers and logging additional information:

```shell
python3 -m tools.run -d filter -A
```

Furthermore, the arguments can be combined to show which criteria specific providers do not meet for being in a specific category.

Example for creating a list of category **A** providers out of `example.org` and `example.com` and logging additional information:

```shell
python3 -m tools.run -d filter -A example.org example.com
```

## Badge Tool

The tool `tools/badge.py` can be used to create badges for the specified categories.
It uses the files created by the filter tool.
Thus, the filter tool must be run before running the badge tool.

Template files are used for generating the badges.
All badges contain the category.
Additionally, it is possible to create badges containing the count of providers in a specific category.

The badge tool has two phases:

1. It generates badges for all categories.
1. It creates the directory `badges` and fills it with badges for all providers (called **provider badges**).

### Badge Tool Usage

Badges for all categories are created if you run the tool without arguments:

```shell
python3 -m tools.run badge
```

The badges for providers can be created as symbolic **l**inks instead of regular files:

```shell
python3 -m tools.run badge -l
```

If you want to create a badge which can be used by a provider, simply enter the category name.

Example for creating a badge for category **A**:

```shell
python3 -m tools.run badge -A
```

Badges containing the **c**ount of providers in specific categories are also possible.

Example for creating a badge containing the count of providers in category **C** in addition to the normal badges:

```shell
python3 -m tools.run badge -c -C
```

## URL Checker

The tool `tools/check_urls.py` can be used to check URLs.
If an unreachable URL is found, the tool quits with the (error) exit code `1`.

### URL Checker Usage

The URLs of all appropriate files are checked if you run the tool without arguments:

```shell
python3 -m tools.run check_urls
```

It is also possible to check only the URLs in specific files.

Example for checking the URLs in `providers.json` and `tools/filter.py`:

```shell
python3 -m tools.run check_urls providers.json tools/filter.py
```

## Graphic Rendering

The tool `tools/render_graphics.sh` produces PNG files out of SVG files.
All created PNG files are stored in the directory `rendered-graphics` which is created when the rendering starts.
The [XMPP Providers logo](/logo.svg) is used as the basis for various avatar PNG files.

### Dependencies

In order to run the tool, the following dependencies must be installed:

* [Inkscape](https://inkscape.org) to render the graphics
* [Trimage](https://trimage.org) to optimize the rendered graphics

### Usage

All files are rendered if you run the tool without arguments:

```shell
tools/render_graphics.sh
```

## Automation

The providers file is automatically kept up up-do-date via **automation tools**.
They retrieve data from external sources and write changes to the providers file.

### GitLab Bot

The [GitLab bot](tools/automation/gitlab.py) can be used to automatically gather updates of the providers file and create commits for them.
It runs all related bots and aggregates their changes.

#### GitLab Bot Usage

You need to specify the Git *branch* on that the bot should run and to that it pushes the created commits.
Furthermore, the credentials for the GitLab bot itself and for the XMPP bot are needed.

The bot needs to be a member of the main repository with the role *Developer* in order to push to it.
If the branch is [protected](https://invent.kde.org/melvo/xmpp-providers/-/settings/repository#js-protected-branches-settings), pushing to it needs to be explicitly allowed for *Developers*.
In order to create commits via the bot's GitLab user, the tool needs the bot's [personal API token](https://invent.kde.org/-/profile/personal_access_tokens):

```shell
python3 -m tools.run gitlab_bot "<branch>" "<bot personal token>" "<XMPP bot JID>" "<XMPP bot password>"
```

Example:

```shell
python3 -m tools.run gitlab_bot "master" "glghh-jasjlkadg8ADSFGGasfh" "bot@example.org" "74UA4kj8sN5mnf5PXbZ5"
```

#### GitLab Bot Continuous Integration (CI)

The GitLab bot runs regularly via GitLab's CI.
If it determines changes of the providers' properties, it creates commits for them on the default branch and on all stable branches (i.e., branches with the prefix `stable/` - e.g., `stable/v1`).

The CI passes the [environment variables](https://invent.kde.org/melvo/xmpp-providers/-/settings/ci_cd#js-cicd-variables-settings) `GITLAB_BOT_TOKEN`, `XMPP_BOT_JID` and `XMPP_BOT_PASSWORD` via its [configuration file](.gitlab-ci.yml) as command-line arguments to the GitLab bot.
Those variables must be **protected** and **masked** in order to restrict their access.
Protected variables can only be accessed by pipelines for protected branches.
Thus, each branch on that the bot is run must be protected.

A [scheduled pipeline](https://invent.kde.org/melvo/xmpp-providers/-/pipeline_schedules) starts the GitLab bot.
It needs the variable `SCHEDULED_GITLAB_BOT` with the value `true` to ensure that the corresponding job is only run within the scheduled pipeline.
You can pass options to `tools.run` via the variable `BASE_TOOL_OPTIONS` (e.g., `-d` to run it in debug mode).
Those variables are passed to the CI via its configuration file.

### Web Bot

The [web bot](tools/automation/web.py) can be used to query provider ratings of external services and properties from [provider files](README.md#provider-file).

#### Web Bot Usage

All ratings are queried and the corresponding properties in the providers file changed (when necessary) if you run the tool without arguments:

```shell
python3 -m tools.run web_bot
```

You can also trigger **u**pdates of ratings supporting that (querying and updating a rating cannot be done at the same time):

```shell
python3 -m tools.run web_bot -u
```

The bot can run only for specific providers as well:

```shell
python3 -m tools.run web_bot example.org example.com
```

#### Web Bot Continuous Integration (CI)

The web bot runs regularly via GitLab's CI.
It triggers updates of ratings ensuring that the GitLab bot receives the latest changes.

A scheduled pipeline starts the web bot.
It needs the variable `SCHEDULED_WEB_BOT` with the value `true`.
That variable is passed to the CI via its configuration file and ensures that the corresponding job is only run within the scheduled pipeline.

### XMPP Bot

The [XMPP bot](tools/automation/xmpp.py) can be used to retrieve properties from providers.
If queried properties contain changed values, those are reflected in the providers file.

There are two kinds of checks covered by the bot.
The first one discovers properties by sending authenticated requests (i.e., via an XMPP account).
The second one handles properties that can be determined by sending unauthenticated requests (i.e., without an XMPP account).

#### Authenticated Requests

The bot can retrieve publicly-accessible information from the providers' servers.
For example, the property `maximumHttpFileUploadFileSize` is fetched as specified by [HTTP File Upload](https://xmpp.org/extensions/xep-0363.html#disco).

#### Unauthenticated Requests

The bot can determine some properties via [In-Band Registration](https://xmpp.org/extensions/xep-0077.html) (*IBR*).
It requests each server to create an account on it.
If that is denied by the server, the property `inBandRegistration` is set to `false`.

If a server allows registrations, the bot sends requested data to the server.
In case of a successful registration, the corresponding property is set to `true`.
Afterwards, created accounts are deleted from the server.

The bot cannot complete CAPTCHAs.
Thus, if the server requires a CAPTCHA and rejects an account creation due to a wrong CAPTCHA response, `inBandRegistration` is set to `true`.
That means, it is assumed that servers support account creation if they use CAPTCHAs.
Whether that CAPTCHA is actually solvable by humans cannot be determined by the bot.
That case needs a manual check.

Furthermore, the property `registrationWebPage` can be retrieved via IBR if the server defined a [redirection URL](https://xmpp.org/extensions/xep-0077.html#redirect).
That check does not depend on whether the server supports account creation via IBR.
The URL is taken in both cases as long as the server provides it.

Servers do not need to announce whether they allow account creation via IBR or not.
Additionally, there are servers that seem to allow account creation via IBR but respond with an error when they receive the data they requested.
Thus, the bot always tries to create an account and reacts to the servers' responses accordingly.
The goal is to simulate human users and various XMPP clients by that behavior.

#### XMPP Bot Usage

For requesting properties via the bot's XMPP account, the tool needs the bot's XMPP credentials.
You can run all checks:

```shell
python3 -m tools.run xmpp_bot "<bot JID>" "<bot password>"
```

Example:

```shell
python3 -m tools.run xmpp_bot "bot@example.org" "74UA4kj8sN5mnf5PXbZ5"
```

The bot can run only for specific providers as well:

```shell
python3 -m tools.run xmpp_bot "bot@example.org" "74UA4kj8sN5mnf5PXbZ5" example.org example.com
```
