<!--
SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Artifacts

[TOC]

## Overview

There are various GitLab CI jobs that provide files (**artifacts**) to be used by external services.
Those artifacts can be retrieved by a content service, such as `data.xmpp.net`, for offering them to services like the [XMPP Providers website](https://providers.xmpp.net).
The following sections describe which artifacts are provided and how they can be fetched.

In order to keep the load for GitLab as low as possible, two kinds of external services are used:

1. A **content service** periodically retrieves the artifacts from GitLab
1. A (regular) **service** uses (downloads/references) the data it needs of a content service

## Unmaintained Versions

**This section is only for project members!**

For some old versions, the corresponding artifacts are not updated any longer via a [scheduled GitLab bot pipeline](TOOLS.md#gitlab-bot-continuous-integration-ci).
Since artifacts are automatically removed after a given expiration period, a content service would afterwards not be able to retrieve the latest artifacts anymore.
To avoid that, click the **Keep** button on each relevant GitLab CI job's page in the **Job artifacts** section as soon as the corresponding scheduled GitLab bot pipeline is removed.

## Providers

The **providers file** contains all provider data (including sources and comments).
**Provider lists** are created by filtering the providers file and contain only the property content.
All are JSON files.

### Provider Lists

#### Single Provider Lists

If you want to provide a service based on XMPP Providers, in most cases the [filtered provider lists](/README.md#usage) should be sufficient.

A content service can retrieve the filtered provider lists from the corresponding job (replace `<version>` and `<list>` with the desired values, where `<list>` can be one of `A`, `B`, `C`, `D`, `As`, `Bs`, `Cs`, `Ds`):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/raw/providers-<list>.json?job=filtered-provider-lists
```

Example for a **s**imple provider list of category **D** and the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/raw/providers-Ds.json?job=filtered-provider-lists
```

#### All Provider Lists

If you want to provide a service based on XMPP Providers, you can get all provider lists (replace `<version>` with the desired value):

```plaintext
https://data.xmpp.net/providers/v<version>/providers.zip
```

Example for all provider lists of the latest version **2**:

```plaintext
https://data.xmpp.net/providers/v2/providers.zip
```

A content service can retrieve all provider lists from the corresponding job by fetching the whole artifacts archive and extracting its top-level files without any subdirectories (replace `<version>` with the desired value):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/download/?job=filtered-provider-lists
```

Example for all provider lists of the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/download/?job=filtered-provider-lists
```

### Providers File

If a service needs all data (including sources and comments), the providers file can be fetched as well (replace `<version>` with the desired value:

```plaintext
https://data.xmpp.net/providers/v<version>/providers.json
```

Example for a providers file of the latest version **2**:

```plaintext
https://data.xmpp.net/providers/v2/providers.json
```

A content service can retrieve the providers file from the corresponding job (replace `<version>` with the desired value):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/raw/providers.json?job=original-files
```

Example for a providers file of the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/raw/providers.json?job=original-files
```

## Clients

Clients integrating XMPP Providers are listed in a clients file.

If you want to provide a service based on XMPP Providers, you can get the clients file (replace `<version>` with the desired value):

```plaintext
https://data.xmpp.net/providers/v<version>/clients.json
```

Example for the clients file of the latest version **2**:

```plaintext
https://data.xmpp.net/providers/v2/clients.json
```

A content service can retrieve the clients file from the corresponding job (replace `<version>` with the desired value):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/raw/clients.json?job=original-files
```

Example for the clients file of the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/raw/clients.json?job=original-files
```

## Badges

There are two kinds of badges.
A provider badge is used to show a provider's category.
A count badge shows the count of providers in a category.

### Provider Badges

An XMPP provider can embed a badge showing its category in its website.
The details pages on the [XMPP Providers website](https://providers.xmpp.net) contain the code to be embedded.
The badges are SVG files.

#### Single Provider Badges

If you want to provide a service based on XMPP Providers, you can get single provider badges (replace `<version>` and `<provider>` with the desired values):

```plaintext
https://data.xmpp.net/providers/v<version>/badges/<provider>.svg
```

Example for a provider badge of `example.org` and the latest version **2**:

```plaintext
https://data.xmpp.net/providers/v2/badges/example.org.svg
```

A content service can retrieve single provider badges from the corresponding job (replace `<version>` and `<provider>` with the desired values):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/raw/badges/<provider>.svg?job=badges
```

Example for a provider badge of `example.org` and the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/raw/badges/example.org.svg?job=badges
```

#### All Provider Badges

If you want to provide a service based on XMPP Providers, you can get all provider badges (replace `<version>` with the desired value):

```plaintext
https://data.xmpp.net/providers/v<version>/provider-badges.zip
```

Example for all provider badges of the latest version **2**:

```plaintext
https://data.xmpp.net/providers/v2/provider-badges.zip
```

A content service can retrieve all provider badges from the corresponding job by fetching the whole artifacts archive and extracting its `badges` directory (replace `<version>` with the desired value):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/download/?job=badges
```

Example for all provider badges of the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/download/?job=badges
```

### Count Badges

A count badge shows how many providers are in a specific category.
All count badges are displayed at the top of the [README](/README.md).
The badges are SVG files.

#### Single Count Badges

If you want to provide a service based on XMPP Providers, you can get single count badges (replace `<version>` and `<category>` with the desired values):

```plaintext
https://data.xmpp.net/providers/v<version>/count-badge-<category>.svg
```

Example for a count badge of category **A** and the latest version **2**:

```plaintext
https://data.xmpp.net/providers/v2/count-badge-A.svg
```

A content service can retrieve single count badges from the corresponding job (replace `<version>` and `<category>` with the desired values):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/raw/count-badge-<category>.svg?job=badges
```

Example for a count badge of category **A** and the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/raw/count-badge-A.svg?job=badges
```

#### All Count Badges

If you want to provide a service based on XMPP Providers, you can get all count badges (replace `<version>` with the desired value):

```plaintext
https://data.xmpp.net/providers/v<version>/count-badges.zip
```

Example for all count badges of the latest version **2**:

```plaintext
https://data.xmpp.net/providers/v2/count-badges.zip
```

A content service can retrieve all count badges from the corresponding job by fetching the whole artifacts archive and extracting its top-level files without any subdirectories (replace `<version>` with the desired value):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/download/?job=badges
```

Example for all count badges of the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/download/?job=badges
```

## Results

The categorization results can be used to determine why a provider is not in a specific category.
The details pages on the [XMPP Providers website](https://providers.xmpp.net) use those results to explain why a provider is not in category **A**.
The results are stored in a JSON file that lists all properties that do not meet the criteria for being in a specific category.

### Single Results

If you want to provide a service based on XMPP Providers, you can get single results (replace `<version>` and `<provider>` with the desired values):

```plaintext
https://data.xmpp.net/providers/v<version>/results/<provider>.json
```

Example for a result of `example.org` and the latest version **2**:

```plaintext
https://data.xmpp.net/providers/v2/results/example.org.json
```

A content service can retrieve single results from the corresponding job (replace `<version>` and `<provider>` with the desired values):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/raw/results/<provider>.json?job=filtered-provider-lists
```

Example for a result of `example.org` and the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/raw/results/example.org.json?job=filtered-provider-lists
```

### All Results

If you want to provide a service based on XMPP Providers, you can get all results (replace `<version>` with the desired value):

```plaintext
https://data.xmpp.net/providers/v<version>/results.zip
```

Example for all results of the latest version **2**:

```plaintext
https://data.xmpp.net/providers/v2/results.zip
```

A content service can retrieve all results from the corresponding job by fetching the whole artifacts archive and extracting its `results` directory (replace `<version>` with the desired value):

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v<version>/download/?job=filtered-provider-lists
```

Example for all results of the latest version **2**:

```plaintext
https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/stable/v2/download/?job=filtered-provider-lists
```
