#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
# SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains methods for working with the properties of providers."""

from __future__ import annotations

from typing import Any
from typing import cast

import logging
import os
from enum import Enum
from subprocess import call

from tools.common import convert_python_data_to_json_string
from tools.common import CRITERIA_FILE_PATH
from tools.common import DATA_TYPES_FILE_PATH
from tools.common import load_json_file
from tools.common import PROVIDER_PROPERTIES_FILE_PATH
from tools.common import PROVIDERS_FILE_PATH
from tools.common import README_FILE_PATH
from tools.data_types import ProviderDetailsT
from tools.prettify import Prettify
from tools.prettify import PropertyType

ACTION_ADD_PROPERTY = "a"
ACTION_MOVE_PROPERTY = "m"
ACTION_RENAME_PROPERTY = "r"
ACTION_DELETE_PROPERTY = "d"
ACTION_QUIT = "q"
STRING_TRUE = "true"
STRING_FALSE = "false"

log = logging.getLogger()


class InputType(Enum):
    """Data types for user input."""

    STRING = 1
    INTEGER = 2
    BOOLEAN = 3
    LIST = 4
    DICTIONARY = 5
    NULL = 6


class PropertyManager:
    """Manages the properties that providers have."""

    def __init__(self) -> None:
        while True:
            action = self._get_action()

            if action == ACTION_ADD_PROPERTY:
                self._show_property_addition_dialog()
            elif action == ACTION_MOVE_PROPERTY:
                self._show_property_moving_dialog()
            elif action == ACTION_RENAME_PROPERTY:
                self._show_property_renaming_dialog()
            elif action == ACTION_DELETE_PROPERTY:
                self._show_property_deletion_dialog()
            elif action.lower() == ACTION_QUIT:
                self._clear()
                break
            else:
                self._show_title_bar()
                print("\nUnknown action.\n")
                self._show_continue_prompt()

    def _show_property_addition_dialog(self) -> None:
        self._show_title_bar()
        print("Action: Add property\n")

        input_type = self._get_input_type()

        self._add_property(
            self._get_new_property_position_for_addition(),
            self._get_property_name(),
            input_type,
            self._is_source_to_be_added(),
            self._get_property_default(input_type),
        )
        self._show_continue_prompt()

        self._show_title_bar()

    def _show_property_moving_dialog(self) -> None:
        self._show_title_bar()
        print("Action: Move property\n")

        self._move_property(
            *self._get_property_position_and_name_for_moving(),
            self._get_new_property_position_for_moving(),
        )
        self._show_continue_prompt()

        self._show_title_bar()

    def _show_property_renaming_dialog(self) -> None:
        self._show_title_bar()
        print("Action: Rename property\n")

        self._rename_property(
            *self._get_property_position_and_name_for_renaming(),
            self._get_new_property_name(),
        )
        self._show_continue_prompt()

        self._show_title_bar()

    def _show_property_deletion_dialog(self) -> None:
        self._show_title_bar()
        print("Action: Delete property\n")

        self._delete_property(*self._get_property_position_and_name_for_deletion())
        self._show_continue_prompt()

        self._show_title_bar()

    def _add_property(
        self,
        position: int,
        name: str,
        content_type: InputType,
        add_source: bool,
        default: None | int | bool = None,
    ) -> None:
        """Adds a new property with a name, type and default value.

        Parameters
        ----------
        position : int
            index of the new property
        name : str
            name of the new property
        content_type : InputType
            data type of the new property's content
        add_source : bool
            whether to add a 'source' key for the new property
        default : None | int | bool
            default value for the new property (only used for integers and booleans)
        """

        self._show_title_bar()
        print("Action: Add property\n")

        if not self._add_property_to_properties_file(position, name):
            print(f"Could not add '{name}' to '{PROVIDER_PROPERTIES_FILE_PATH}'")
            return

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        providers = cast(dict[str, ProviderDetailsT], json_data)

        if default is None:
            value = self._convert_input_type_to_python_type(content_type)
        else:
            value = default

        for provider in providers.values():
            if add_source:
                provider[name] = {
                    "content": value,
                    "source": "",
                    "comment": "",
                }
            else:
                provider[name] = {
                    "content": value,
                    "comment": "",
                }

        try:
            with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
                providers_file.write(convert_python_data_to_json_string(providers))
        except OSError as e:
            print(
                f"The property '{name}' could not be added to '{PROVIDERS_FILE_PATH}': "
                f"{e}"
            )
            return

        if not self._sort_properties_within_providers_file():
            return

        print(f"Property '{name}' has been added.")
        print(
            f"Make sure to add corresponding documentation to '{README_FILE_PATH}', "
            f"criteria to '{CRITERIA_FILE_PATH}' and data types to "
            f"'{DATA_TYPES_FILE_PATH}'"
        )
        self._show_new_version_required_hint()

    def _move_property(self, old_position: int, name: str, new_position: int) -> None:
        """Moves a property within the properties file (by its position) and within each
        provider in the providers file (by its name).

        Parameters
        ----------
        old_position : int
            old index of the property to be moved
        name : str
            name of the property to be moved
        new_position : int
            new index of the property to be moved
        """

        self._show_title_bar()
        print("Action: Move property\n")

        if not self._move_property_within_properties_file(
            old_position, name, new_position
        ):
            print(f"Could not move '{name}' within '{PROVIDER_PROPERTIES_FILE_PATH}'")
            return

        if not self._sort_properties_within_providers_file():
            return

        print(f"Property '{name}' has been moved.")
        print(
            f"Make sure to move the property's documentation within "
            f"'{README_FILE_PATH}', corresponding criteria within "
            f"'{CRITERIA_FILE_PATH}' and data types within '{DATA_TYPES_FILE_PATH}'"
        )

    def _rename_property(self, position: int, old_name: str, new_name: str) -> None:
        """Renames a property within the properties file (by its position) and within
        each provider in the providers file (by its name).

        Parameters
        ----------
        position : int
            index of the property to be renamed
        old_name : str
            old name of the property to be renamed
        new_name : str
            new name of the property to be renamed
        """

        self._show_title_bar()
        print("Action: Rename property\n")

        if not self._rename_property_within_properties_file(
            position, old_name, new_name
        ):
            print(
                f"Could not rename '{old_name}' to '{new_name}' within "
                f"'{PROVIDER_PROPERTIES_FILE_PATH}'"
            )
            return

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        providers = cast(dict[str, ProviderDetailsT], json_data)

        for provider in providers.values():
            try:
                provider[new_name] = provider.pop(
                    old_name  # pyright: ignore [reportArgumentType]
                )
            except KeyError:
                print(
                    f"Could not rename '{old_name}' to '{new_name}' within "
                    f"'{PROVIDERS_FILE_PATH}' because the property does not exist in "
                    f"that file"
                )
                return

        try:
            with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
                providers_file.write(convert_python_data_to_json_string(providers))
        except OSError as e:
            print(
                f"The property '{old_name}' could not be renamed to '{new_name}' "
                f"within '{PROVIDERS_FILE_PATH}': {e}"
            )
            return

        if not self._sort_properties_within_providers_file():
            return

        print(f"Property '{old_name}' has been renamed to '{new_name}'.")
        print(
            f"Make sure to rename the property in its documentation within "
            f"'{README_FILE_PATH}', corresponding criteria within "
            f"'{CRITERIA_FILE_PATH}' and data types within '{DATA_TYPES_FILE_PATH}'"
        )
        self._show_new_version_required_hint()

    def _delete_property(self, position: int, name: str) -> None:
        """Deletes a property from the properties file (by its position) and from each
        provider in the providers file (by its name).

        Parameters
        ----------
        position : int
            index of the property to be deleted
        name : str
            name of the property to be deleted
        """

        self._show_title_bar()
        print("Action: Delete property\n")

        if not self._delete_property_from_properties_file(position, name):
            print(f"Could not delete '{name}' from '{PROVIDER_PROPERTIES_FILE_PATH}'")
            return

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        providers = cast(dict[str, ProviderDetailsT], json_data)

        for provider in providers.values():
            try:
                provider.pop(name)  # pyright: ignore [reportArgumentType]
            except KeyError:
                print(
                    f"Could not delete '{name}' from '{PROVIDERS_FILE_PATH}' because "
                    f"the property does not exist in that file"
                )
                return

        try:
            with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
                providers_file.write(convert_python_data_to_json_string(providers))
        except OSError as e:
            print(
                f"The property '{name}' could not be deleted from "
                f"'{PROVIDERS_FILE_PATH}': {e}"
            )
            return

        print(f"Property '{name}' has been deleted.")
        print(
            f"Make sure to delete the property's documentation from "
            f"'{README_FILE_PATH}', corresponding criteria from "
            f"'{CRITERIA_FILE_PATH}' and data types from '{DATA_TYPES_FILE_PATH}'"
        )
        self._show_new_version_required_hint()

    def _add_property_to_properties_file(
        self,
        position: int,
        name: str,
    ) -> bool:
        """Adds a property to the properties file.

        Parameters
        ----------
        position : int
            index of the property to be added
        name : str
            name of the property to be added

        Returns
        -------
        bool
            whether the addition succeeded
        """

        json_data = load_json_file(PROVIDER_PROPERTIES_FILE_PATH)

        if json_data is None:
            return False

        properties = cast(list[str], json_data)
        properties.insert(position, name)

        try:
            with open(PROVIDER_PROPERTIES_FILE_PATH, "w") as properties_file:
                properties_file.write(convert_python_data_to_json_string(properties))

        except OSError as e:
            print(
                f"Property '{name}' could not be added to "
                f"'{PROVIDER_PROPERTIES_FILE_PATH}' because that file could not be "
                f"processed: {e}"
            )
            return False

        return True

    def _move_property_within_properties_file(
        self, old_position: int, name: str, new_position: int
    ) -> bool:
        """Moves a property within the properties file from its old position to a new
        one.

        Parameters
        ----------
        old_position : int
            old index of the property to be moved
        name : str
            name of the property to be moved
        new_position : int
            new index of the property to be moved

        Returns
        -------
        bool
            whether the move succeeded
        """

        json_data = load_json_file(PROVIDER_PROPERTIES_FILE_PATH)

        if json_data is None:
            return False

        properties = cast(list[str], json_data)
        old_item = properties.pop(old_position)
        new_position = new_position - 1 if old_position < new_position else new_position
        properties.insert(new_position, old_item)

        try:
            with open(PROVIDER_PROPERTIES_FILE_PATH, "w") as properties_file:
                properties_file.write(convert_python_data_to_json_string(properties))

        except OSError as e:
            print(
                f"Property '{name}' could not be moved within "
                f"'{PROVIDER_PROPERTIES_FILE_PATH}' because that file could not be "
                f"processed: {e}"
            )
            return False

        return True

    def _rename_property_within_properties_file(
        self, position: int, old_name: str, new_name: str
    ) -> bool:
        """Renames a property within the properties file.

        Parameters
        ----------
        position : int
            index of the property to be renamed
        old_name : str
            old name of the property to be renamed
        new_name : int
            new name of the property to be renamed

        Returns
        -------
        bool
            whether the renaming succeeded
        """

        json_data = load_json_file(PROVIDER_PROPERTIES_FILE_PATH)

        if json_data is None:
            return False

        properties = cast(list[str], json_data)
        properties[position] = new_name

        try:
            with open(PROVIDER_PROPERTIES_FILE_PATH, "w") as properties_file:
                properties_file.write(convert_python_data_to_json_string(properties))

        except OSError as e:
            print(
                f"Property '{old_name}' could not be renamed to '{new_name}' within "
                f"'{PROVIDER_PROPERTIES_FILE_PATH}' because that file could not be "
                f"processed: {e}"
            )
            return False

        return True

    def _delete_property_from_properties_file(self, position: int, name: str) -> bool:
        """Deletes a property from the properties file by its position.

        Parameters
        ----------
        position : int
            index of the property to be deleted
        name : str
            name of the property to be deleted

        Returns
        -------
        bool
            whether the deletion succeeded
        """

        json_data = load_json_file(PROVIDER_PROPERTIES_FILE_PATH)

        if json_data is None:
            return False

        properties = cast(list[str], json_data)
        properties.pop(position)

        try:
            with open(PROVIDER_PROPERTIES_FILE_PATH, "w") as properties_file:
                properties_file.write(convert_python_data_to_json_string(properties))

        except OSError as e:
            print(
                f"Property '{name}' could not be deleted from "
                f"'{PROVIDER_PROPERTIES_FILE_PATH}' because that file could not be "
                f"processed: {e}"
            )
            return False

        return True

    def _sort_properties_within_providers_file(self) -> bool:
        """Moves the properties in the providers file to their positions specified in
        the properties file.

        Returns
        -------
        bool
            whether the properties could be sorted
        """

        try:
            log_level = log.getEffectiveLevel()
            log.setLevel(logging.ERROR)
            Prettify({PropertyType.PROVIDER: [PROVIDERS_FILE_PATH]})
            log.setLevel(log_level)

        except OSError as e:
            print(
                f"The properties in '{PROVIDERS_FILE_PATH}' could not be sorted as "
                f"specified by '{PROVIDER_PROPERTIES_FILE_PATH}': {e}"
            )
            return False

        except SystemExit as e:
            print(
                f"The properties in '{PROVIDERS_FILE_PATH}' could not be sorted as "
                f"specified by '{PROVIDER_PROPERTIES_FILE_PATH}': {e}"
            )
            return False

        return True

    def _show_continue_prompt(self) -> None:
        """Shows a prompt until any key is pressed."""

        input("\nPress any key to continue...")

    def _clear(self) -> None:
        """Clears the terminal's content."""

        call("clear" if os.name == "posix" else "cls", shell=False)  # noqa: S603

    def _get_action(self) -> str:
        """Prompts for a user action (main menu)."""

        self._show_title_bar()
        print("What would you like to do?")
        print("[%s] Add property" % ACTION_ADD_PROPERTY)
        print("[%s] Move property" % ACTION_MOVE_PROPERTY)
        print("[%s] Rename property" % ACTION_RENAME_PROPERTY)
        print("[%s] Delete property" % ACTION_DELETE_PROPERTY)
        print("[%s] Quit" % ACTION_QUIT)

        return input("Action: ")

    def _get_new_property_position_for_addition(self) -> int:
        """Prompts for choosing the position of a property to be added.

        Returns
        -------
        int
            index of the new property
        """

        return self._get_new_property_position(
            "Move property", "Where should the property be positioned?"
        )

    def _get_new_property_position_for_moving(self) -> int:
        """Prompts for choosing the new position of an existing property.

        Returns
        -------
        int
            new index of the existing property
        """

        return self._get_new_property_position(
            "Add property", "Where should the new property be positioned?"
        )

    def _get_new_property_position(
        self, action_title: str, action_question: str
    ) -> int:
        """Prompts for choosing the position of a property to be used by an action.

        Parameters
        ----------
        action_title : str
            title to be shown for the specific action
        action_question : str
            question to be asked for doing the action

        Returns
        -------
        int
            index of the property
        """

        json_data = load_json_file(PROVIDER_PROPERTIES_FILE_PATH)

        if json_data is None:
            return False

        properties = cast(list[str], json_data)
        possible_positions = "[0]"

        for i, property_name in enumerate(properties):
            possible_positions += f" {property_name}\n[{i + 1}]"

        while True:
            self._show_title_bar()
            print(f"Action: {action_title}\n")
            print(f"{action_question}\n")
            print(possible_positions)

            try:
                position = int(input("\nInput (number in braces): "))

                if position in range(len(properties) + 1):
                    return position

            except ValueError:
                pass

    def _get_property_position_and_name_for_moving(self) -> tuple[int, str]:
        """Prompts for choosing an existing property to be moved.

        Returns
        -------
        tuple[int, str]
            index and name of the choosen property
        """

        return self._get_property_position_and_name(
            "Move property", "Which property should be moved?"
        )

    def _get_property_position_and_name_for_renaming(self) -> tuple[int, str]:
        """Prompts for choosing an existing property to be renamed.

        Returns
        -------
        tuple[int, str]
            index and name of the choosen property
        """

        return self._get_property_position_and_name(
            "Rename property", "Which property should be renamed?"
        )

    def _get_property_position_and_name_for_deletion(self) -> tuple[int, str]:
        """Prompts for choosing an existing property to be deleted.

        Returns
        -------
        tuple[int, str]
            index and name of the choosen property
        """

        return self._get_property_position_and_name(
            "Delete property", "Which property should be deleted?"
        )

    def _get_property_position_and_name(
        self, action_title: str, action_question: str
    ) -> tuple[int, str]:
        """Prompts for choosing an existing property to be deleted.

        Parameters
        ----------
        action_title : str
            title to be shown for the specific action
        action_question : str
            question to be asked for doing the action

        Returns
        -------
        tuple[int, str]
            index and name of the choosen property
        """

        json_data = load_json_file(PROVIDER_PROPERTIES_FILE_PATH)

        if json_data is None:
            return -1, ""

        properties = cast(list[str], json_data)
        possible_positions = ""

        for i, property_name in enumerate(properties):
            possible_positions += f"[{i}] {property_name}\n"

        while True:
            self._show_title_bar()
            print(f"Action: {action_title}\n")
            print(f"{action_question}\n")
            print(possible_positions)

            try:
                position = int(input("Input (number in braces): "))

                if position in range(len(properties)):
                    return position, properties[position]

            except ValueError:
                pass

    def _get_property_name(self) -> str:
        """Prompts for a name of a new property."""

        name = ""

        while not name:
            self._show_title_bar()
            print("Action: Add property\n")
            name = input("Property name: ")

        return name

    def _get_new_property_name(self) -> str:
        """Prompts for a new property's name."""

        name = ""

        while not name:
            self._show_title_bar()
            print("Action: Rename property\n")
            name = input("New property name: ")

        return name

    def _get_input_type(self) -> InputType:
        """Prompts for a new property's data type."""

        while True:
            self._show_title_bar()
            print("Action: Add property\n")

            for input_type in InputType:
                print(f"[{input_type.value}] {input_type.name}")

            try:
                return InputType(int(input("\nProperty data type: ")))
            except ValueError:
                pass

    def _get_property_default(self, input_type: InputType) -> Any:
        """Prompts for a new property's default value (used for integers and booleans
        only).
        """

        if input_type in (InputType.INTEGER, InputType.BOOLEAN):
            while True:
                self._show_title_bar()
                print("Action: Add property\n")

                value = input("Default value ( 'False', '-1' etc.): ")
                is_valid, value = self._validate_input_type(value, input_type)

                if is_valid:
                    return value

        return None

    def _is_source_to_be_added(self) -> bool:
        """Prompts whether to add a 'source' field"""

        self._show_title_bar()
        print("Action: Add property\n")
        print("Should a 'source' field be added to the property?\n")

        return input("Input (Y/n): ").lower() != "n"

    def _show_title_bar(self) -> None:
        """Shows the programm's title bar."""

        self._clear()
        print(50 * "=")
        print("Manager for provider properties")
        print(50 * "=")

    def _convert_input_type_to_python_type(self, input_type: InputType) -> Any:
        """Converts a data type from user input to a Python data type.

        Parameters
        ----------
        input_type : InputType
            data type for which to return a Python data type

        Returns
        -------
        Any
            initialized Python data type corresponding to input_type
        """

        if input_type == InputType.STRING:
            return ""
        if input_type == InputType.LIST:
            return []
        if input_type == InputType.DICTIONARY:
            return {}

        return None

    def _validate_input_type(
        self, value: str, input_type: InputType
    ) -> tuple[bool, None | int | bool]:
        """Validates input of value with input_type.

        Parameters
        ----------
        value : str
            value to validate
        input_type : InputType
            data type to validate against

        Returns
        -------
        tuple[bool, None | int | bool]
            validation success and value as a Python data type
        """

        if input_type == InputType.INTEGER:
            try:
                return True, int(value)
            except ValueError:
                return False, None

        if input_type == InputType.BOOLEAN:
            if value.lower() == STRING_TRUE:
                return True, True
            if value.lower() == STRING_FALSE:
                return True, False

        return False, None

    @staticmethod
    def _show_new_version_required_hint() -> None:
        """Shows a hint to release a new version for the changes."""

        print(
            "IMPORTANT: This change requires a new version branch 'stable/v<version>' "
            "(with `<version>` replaced by the new version)!"
        )
