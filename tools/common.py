#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains code needed by various tools."""

from typing import Any
from typing import ClassVar

import json
import logging
from enum import Enum
from logging import Formatter
from logging import LogRecord
from pathlib import Path

GITLAB_HOST = "invent.kde.org"

README_FILE_PATH = "README.md"
CONTRIBUTING_FILE_PATH = "CONTRIBUTING.md"
CRITERIA_FILE_PATH = "tools/criteria.py"
DATA_TYPES_FILE_PATH = "tools/data_types.py"
PROVIDER_PROPERTIES_FILE_PATH = "provider-properties.json"
CLIENT_PROPERTIES_FILE_PATH = "client-properties.json"
PROVIDER_FILE_SCHEMA_FILE_PATH = "provider-file-schema.json"
PROVIDERS_FILE_PATH = "providers.json"
CLIENTS_FILE_PATH = "clients.json"

BADGE_COMMAND = "badge"
CHECK_URLS_COMMAND = "check_urls"
FILTER_COMMAND = "filter"
GITLAB_BOT_COMMAND = "gitlab_bot"
PRETTIFY_COMMAND = "prettify"
PROPERTY_MANAGER_COMMAND = "property_manager"
PROVIDER_MANAGER_COMMAND = "provider_manager"
WEB_BOT_COMMAND = "web_bot"
XMPP_BOT_COMMAND = "xmpp_bot"

JSON_INDENTATION = 4
LOG_FORMAT = "%(asctime)s %(levelname)-8s %(module)-16s %(message)s"
LOG_DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

log = logging.getLogger()


class LogColor(Enum):
    """This contains the colors for log messages."""

    NONE = chr(27) + "[0m"
    BLACK = chr(27) + "[30m"
    RED = chr(27) + "[31m"
    GREEN = chr(27) + "[32m"
    BROWN = chr(27) + "[33m"
    BLUE = chr(27) + "[34m"
    MAGENTA = chr(27) + "[35m"
    CYAN = chr(27) + "[36m"
    LIGHT_GRAY = chr(27) + "[37m"
    DARK_GRAY = chr(27) + "[30;1m"
    BRIGHT_RED = chr(27) + "[31;1m"
    BRIGHT_GREEN = chr(27) + "[32;1m"
    YELLOW = chr(27) + "[33;1m"
    BRIGHT_BLUE = chr(27) + "[34;1m"
    PURPLE = chr(27) + "[35;1m"
    BRIGHT_CYAN = chr(27) + "[36;1m"
    WHITE = chr(27) + "[37;1m"


class ColoredLogFormatter(Formatter):
    """Formats log records."""

    colors: ClassVar = {
        "DEBUG": LogColor.BLUE.value,
        "INFO": LogColor.GREEN.value,
        "WARNING": LogColor.YELLOW.value,
        "ERROR": LogColor.RED.value,
        "CRITICAL": LogColor.BRIGHT_RED.value,
    }

    def __init__(
        self, log_format: str = LOG_FORMAT, log_date_format: str = LOG_DATE_FORMAT
    ) -> None:
        Formatter.__init__(self, log_format, log_date_format)

    def format(self, record: LogRecord) -> str:
        """Formats a log record.

        Parameters
        ----------
        record : LogRecord
            log record to be formatted

        Returns
        -------
        str
            formatted log record (i.e., the log message)
        """

        color = ColoredLogFormatter.colors.get(record.levelname, "")
        formatted_level_name = f"[{record.levelname}]".ljust(10)
        record.levelname = f"{color}{formatted_level_name}{LogColor.NONE.value}"

        return Formatter.format(self, record)


class Category(Enum):
    """This contains the provider categories."""

    ALL = "a"
    AUTOMATICALLY_CHOSEN = "A"
    MANUALLY_SELECTABLE = "B"
    COMPLETELY_CUSTOMIZABLE = "C"
    AUTOCOMPLETE = "D"


def create_parent_directories(file_path: Path) -> None:
    """Creates all parent directories of a file.

    Parameters
    ----------
    file_path : Path
        path of the file
    """

    file_path.parent.mkdir(parents=True, exist_ok=True)


def load_json_file(path: str) -> Any:
    """Loads a JSON file and converts it to a Python object.

    Parameters
    ----------
    path : str
        path of the JSON file being loaded

    Returns
    -------
    Any
        Python object if the file could be opened and converted, otherwise None
    """
    try:
        with open(path) as file:
            return json.load(file)

    except json.JSONDecodeError as e:
        log.exception(
            "File '%s' could not be loaded because of invalid JSON syntax: %s in line "
            "%s at column %s",
            path,
            e.msg,
            e.lineno,
            e.colno,
        )
        return None

    except OSError:
        log.exception("File '%s' could not be loaded", path)
        return None


def convert_python_data_to_json_string(source: Any) -> str:
    """Converts Python data to a JSON string.

    Parameters
    ----------
    source : dict
        source to be converted to a JSON string

    Returns
    -------
    str
        source's data represented as JSON data
    """

    # A newline is appended because Python's JSON module does not add one.
    return json.dumps(source, indent=JSON_INDENTATION) + "\n"
