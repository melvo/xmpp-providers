#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Melvin Keskin <melvo@olomono.de>
# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Checks whether URLs in files are reachable."""

import asyncio
import json
import logging
import os
import re
import sys
from collections import defaultdict
from http import HTTPStatus
from pathlib import Path
from urllib.parse import unquote

import backoff
from aiohttp import ClientError
from aiohttp import ClientSession
from aiohttp import ClientTimeout

from tools.common import PROVIDERS_FILE_PATH

# Files/Directories with those paths are not checked.
EXCLUDED_PATHS = [
    ".git",
    ".gitlab",
    ".ruff_cache",
    ".vscode",
    "badges",
    "logos",
    "results",
    "tools/check_urls.py",
]

# Files with those parts in their paths are not checked (relevant for subpaths).
EXCLUDED_PATH_PARTS = [
    "__init__.py",
    "__pycache__",
    ".venv",
]

# URLs containing those parts are not checked.
EXCLUDED_URL_PARTS = [
    "<version>",
    "example.org",
    "https://%s/.well-known/xmpp-provider-v2.json",
    "https://compliance.conversations.im/server/",
    "https://compliance.conversations.im/live/",
    "https://invent.kde.org/melvo/xmpp-providers/-/settings/access_tokens",
    "https://invent.kde.org/melvo/xmpp-providers/-/settings/ci_cd#js-cicd-variables-settings",
    "https://invent.kde.org/melvo/xmpp-providers/-/settings/repository#js-protected-branches-settings",
    "https://{GITLAB_HOST}",
    "project_host",
]

# Redirects are allowed for URLs with those prefixes.
REDIRECTED_URL_PREFIXES = [
    "https://invent.kde.org/-/profile/personal_access_tokens",
    "https://invent.kde.org/melvo/xmpp-providers.git",
    "https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/",
]

# Redirects are allowed for URLs that are solely in those files/directories.
REDIRECTED_URL_PATHS = [PROVIDERS_FILE_PATH]

URL_REGEX = (
    r'https://[^\s"]*?\)|https://[^\s\)]*?"|https://[^"\;)]*?\s|https://[^\s"\)]*?;'
)

REQUEST_TIMEOUT_SECONDS = 10
MAX_REQUEST_ATTEMPTS = 5

REQUEST_EXCEPTIONS = (asyncio.TimeoutError, ClientError, OSError)

log = logging.getLogger()

# Log only errors which result from giving up after trying MAX_REQUEST_ATTEMPTS times.
logging.getLogger("backoff").setLevel(logging.CRITICAL)


class CheckUrls:
    """Checks whether URLs in specific files or all files (if no files are specified)
    are reachable.

    It quits with the (error) exit code 1 if at least one URL is unreachable.

    Parameters
    ----------
    file_paths : list[str]
        list of relative file paths to be searched for URLs
    """

    def __init__(self, file_paths: list[str]) -> None:
        included_file_paths: list[str] = []
        excluded_file_paths: list[str] = []

        if file_paths:
            for file_path in file_paths:
                if file_path.startswith(tuple(EXCLUDED_PATHS)):
                    excluded_file_paths.append(file_path)
                else:
                    included_file_paths.append(file_path)
        else:
            log.debug("No files specified. Checking all files…")
            included_file_paths, excluded_file_paths = self._get_relevant_paths()

        excluded_file_paths = sorted(excluded_file_paths)
        log.debug("Excluded files/directories: %s", excluded_file_paths)

        if not included_file_paths:
            log.info("Skipping URL check since all files are excluded")
            return

        included_file_paths = sorted(included_file_paths)
        log.info("Starting URL check for: %s", included_file_paths)

        if not asyncio.run(self._check_urls(included_file_paths)):
            sys.exit(1)

    async def _check_urls(self, file_paths: list[str]) -> bool:
        """Searches URLs in files and checks whether the found URLs are reachable.

        Parameters
        ----------
        file_paths : list[str]
            list of relative file paths to be searched for URLs

        Returns
        -------
        bool
            whether all URLs are reachable
        """

        urls: dict[str, dict[str, list[int]]] = defaultdict(lambda: defaultdict(list))

        for file_path in file_paths:
            if not Path(file_path).exists:
                log.warning("File path does not exist: %s", file_path)
                continue

            urls_from_file = self._get_urls_from_file(file_path)

            for url, line_numbers in urls_from_file.items():
                urls[url][file_path] = line_numbers

        tasks: set[asyncio.Task] = set()

        # "raise_for_status=True" is needed to throw an exception for a response within
        # REQUEST_TIMEOUT_SECONDS seconds if its status code is 400 or higher.
        # That is necessary to retry the request for MAX_REQUEST_ATTEMPTS times and to
        # log a warning if no attempt succeeds.
        async with ClientSession(
            timeout=ClientTimeout(total=REQUEST_TIMEOUT_SECONDS),
            raise_for_status=True,
        ) as session, asyncio.TaskGroup() as task_group:
            for url, occurrences in urls.items():
                tasks.add(
                    task_group.create_task(self._check_url(session, url, occurrences))
                )

        return all(task.result() for task in tasks)

    def _get_urls_from_file(self, file_path: str) -> dict[str, list[int]]:
        """Extracts URLs from a file.

        Example return value:
        {
            "https://example.org": [
                "2",
                "5"
            ],
            "https://example.com": [
                "3",
                "4"
            ],
        }

        Parameters
        ----------
        file_path : str
            relative path of file

        Returns
        -------
        dict[str, list[int]]
            extracted URLs and the lines whom contain them
        """

        urls: dict[str, list[int]] = defaultdict(list)

        try:
            with open(file_path) as check_file:
                line_number = 0

                for line in check_file:
                    line_number += 1
                    url_matches = re.findall(URL_REGEX, line)

                    for url_match in url_matches:
                        url = url_match[:-1]

                        if self._is_url_excluded(url, file_path, line_number):
                            continue

                        log.debug(
                            "Inserting for check %s:%s:\t%s",
                            file_path,
                            line_number,
                            url,
                        )

                        urls[url].append(line_number)

        except FileNotFoundError:
            log.exception("File '%s' not found", file_path)

        return urls

    @staticmethod
    def _get_relevant_paths() -> tuple[list[str], list[str]]:
        """Gets all included and excluded paths from the current working directory,
        including paths of files in child directories.

        Only those files are included that are neither in EXCLUDED_PATHS nor in any
        child directory of a directory in EXCLUDED_PATHS.
        If a directory is excluded, its child directories are excluded as well.
        For directories, only the top-most directory of any excluded directory is
        returned.

        Returns
        -------
        tuple[list[str], list[str]]
            relative paths of included files and excluded files/directories
        """

        included_paths: list[str] = []
        excluded_paths: list[str] = []

        for directory_path, _dirs, file_names in os.walk("."):
            # Create a path without the prefix "./".
            simple_directory_path = directory_path[2:]

            # Add excluded parent directories.
            if simple_directory_path in EXCLUDED_PATHS + EXCLUDED_PATH_PARTS:
                excluded_paths.append(simple_directory_path)
                continue

            # Skip excluded child directories of the added parent directories.
            if simple_directory_path.startswith(
                tuple(f"{excluded_path}/" for excluded_path in EXCLUDED_PATHS)
            ):
                continue

            # Add excluded directories with specific path parts.
            if any(
                excluded_path_part in simple_directory_path
                for excluded_path_part in EXCLUDED_PATH_PARTS
            ):
                excluded_paths.append(simple_directory_path)
                continue

            # Process the files of included directories.
            for file_name in file_names:
                # Create a full path for files in (child) directories of the current
                # working directory.
                # Otherwise, use only the filename.
                file_path = (
                    f"{simple_directory_path}/{file_name}"
                    if simple_directory_path
                    else file_name
                )

                # Add excluded files.
                if file_path in EXCLUDED_PATHS or any(
                    excluded_path_part in file_path
                    for excluded_path_part in EXCLUDED_PATH_PARTS
                ):
                    excluded_paths.append(file_path)
                    continue

                # Add included files.
                included_paths.append(file_path)

        return included_paths, excluded_paths

    @staticmethod
    def _is_url_excluded(url: str, file_path: str, line_number: int) -> bool:
        """Checks whether a URL is excluded from the check.
        URLs are excluded when they contain EXCLUDED_URL_PARTS.

        Parameters
        ----------
        url : str
            URL to check
        file_path : str
            path of the file the URL is taken from
        line_number : int
            number of the line containing URL

        Returns
        -------
        bool
            whether the URL is excluded
        """

        for excluded_url_part in EXCLUDED_URL_PARTS:
            if url.find(excluded_url_part) != -1:
                log.debug(
                    "Excluding from check %s:%s:\t%s because it contains '%s'",
                    file_path,
                    line_number,
                    url,
                    excluded_url_part,
                )

                return True

        return False

    async def _check_url(
        self,
        session: ClientSession,
        url: str,
        occurrences: dict[str, list[int]],
    ) -> bool:
        """Starts a request to check whether a URL is reachable.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        url : str
            URL to check
        occurrences : dict[str, list[int]
            files mapped to their lines containing the URL

        Returns
        -------
        bool
            whether the URL is reachable
        """

        log.debug("Checking %s in files/lines: %s", url, json.dumps(occurrences))

        # Allow redirects only for specific URLs.
        redirects_allowed = any(
            url.startswith(prefix) for prefix in REDIRECTED_URL_PREFIXES
        )

        # Allow redirects only for specific files/directories.
        if not redirects_allowed:
            redirects_allowed = all(
                any(occurrence.startswith(path) for path in REDIRECTED_URL_PATHS)
                for occurrence in occurrences
            )

        try:
            return await self._check_status_code(
                session, url, occurrences, redirects_allowed
            )

        except REQUEST_EXCEPTIONS:
            log.warning(
                "Request failed for %s in files/lines: %s: %s",
                url,
                json.dumps(occurrences),
                sys.exc_info()[1],
            )

            return False

    @staticmethod
    @backoff.on_exception(
        backoff.expo, REQUEST_EXCEPTIONS, max_tries=MAX_REQUEST_ATTEMPTS
    )
    async def _check_status_code(
        session: ClientSession,
        url: str,
        occurrences: dict[str, list[int]],
        redirects_allowed: bool,
    ) -> bool:
        """Requests a URL and checks the status code of the response.

        In case of any exception of REQUEST_EXCEPTIONS, the request is retried for
        MAX_REQUEST_ATTEMPTS times without any warning.
        If no attempt succeeds, the exception is thrown to the calling method.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        url : str
            URL to check
        occurrences : dict[str, list[int]]
            files mapped to their lines containing the URL
        redirects_allowed : bool
            whether redirects are treated as reachable URLs

        Returns
        -------
        bool
            whether the URL is reachable
        """

        # Percent-encoded URLs are decoded by "unquote()" because the GET request
        # expects them decoded.
        async with session.get(
            unquote(url), allow_redirects=redirects_allowed
        ) as response:
            status_code = response.status
            log.debug("Received status code %s: %s", status_code, url)

            if status_code != HTTPStatus.OK:
                log.warning(
                    "INVALID status code %s: %s in files/lines: %s",
                    status_code,
                    url,
                    json.dumps(occurrences),
                )

                return False

        return True
