#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
# SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains methods for working with the providers of the providers file."""

from __future__ import annotations

from typing import cast

import copy
import logging
import os
from datetime import datetime
from datetime import UTC
from enum import Enum
from subprocess import call

from tools.common import convert_python_data_to_json_string
from tools.common import load_json_file
from tools.common import PROVIDERS_FILE_PATH
from tools.data_types import ProviderDetailsT
from tools.prettify import Prettify
from tools.prettify import PropertyType

ACTION_ADD_PROVIDER = "a"
ACTION_RENAME_PROVIDER = "r"
ACTION_DELETE_PROVIDER = "d"
ACTION_QUIT = "q"
STRING_TRUE = "true"
STRING_FALSE = "false"

log = logging.getLogger()


class InputType(Enum):
    """Data types for user input."""

    STRING = 1
    INTEGER = 2
    BOOLEAN = 3
    LIST = 4
    DICTIONARY = 5
    NULL = 6


class ProviderManager:
    """Manages the providers."""

    def __init__(self) -> None:
        while True:
            action = self._get_action()

            if action == ACTION_ADD_PROVIDER:
                self._show_provider_addition_dialog()
            elif action == ACTION_RENAME_PROVIDER:
                self._show_provider_rename_dialog()
            elif action == ACTION_DELETE_PROVIDER:
                self._show_provider_deletion_dialog()
            elif action.lower() == ACTION_QUIT:
                self._clear()
                break
            else:
                self._show_title_bar()
                print("\nUnknown action.\n")
                self._show_continue_prompt()

    def _show_provider_addition_dialog(self) -> None:
        self._show_title_bar()
        print("Action: Add provider\n")

        self._add_provider(self._get_new_provider_domain_for_addition())
        self._show_continue_prompt()

        self._show_title_bar()

    def _show_provider_rename_dialog(self) -> None:
        self._show_title_bar()
        print("Action: Rename provider\n")

        old_domain = self._get_provider_domain_for_renaming()
        new_domain = self._get_new_provider_domain_for_renaming(old_domain)
        self._rename_provider(old_domain, new_domain)
        self._show_continue_prompt()

        self._show_title_bar()

    def _show_provider_deletion_dialog(self) -> None:
        self._show_title_bar()
        print("Action: Delete provider\n")

        self._delete_provider(self._get_provider_domain_for_removal())
        self._show_continue_prompt()

        self._show_title_bar()

    def _add_provider(
        self,
        domain: str,
    ) -> None:
        """Adds a new provider with empty properties.

        Parameters
        ----------
        domain : str
            domain of the new provider
        """

        self._show_title_bar()
        print("Action: Add provider\n")

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        providers = cast(dict[str, ProviderDetailsT], json_data)

        first_provider = next(iter(providers.items()))[1]

        if first_provider is None:
            print(
                f"The first provider could not be retrieved to be used as a template "
                f"for creating the new provider '{domain}'"
            )
            return

        provider = copy.deepcopy(first_provider)

        for property_name, property_fields in provider.items():
            for property_field_key, property_field_value in cast(
                dict, property_fields
            ).items():
                if property_field_key == "content":
                    if isinstance(property_field_value, bool):
                        provider[property_name][property_field_key] = False
                    elif isinstance(property_field_value, int):
                        provider[property_name][property_field_key] = -1
                    elif isinstance(property_field_value, str):
                        if property_name in ("latestChange", "since"):
                            provider[property_name][property_field_key] = str(
                                datetime.now(UTC).date()
                            )
                        else:
                            provider[property_name][property_field_key] = ""
                    elif isinstance(property_field_value, list):
                        provider[property_name][property_field_key] = []
                    elif isinstance(property_field_value, dict):
                        provider[property_name][property_field_key] = {}

                elif property_field_key in ("source", "comment"):
                    provider[property_name][property_field_key] = ""

        providers[domain] = provider

        try:
            with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
                providers_file.write(convert_python_data_to_json_string(providers))
        except OSError as e:
            print(
                f"The provider '{domain}' could not be added to "
                f"'{PROVIDERS_FILE_PATH}': {e}"
            )
            return

        try:
            log_level = log.getEffectiveLevel()
            log.setLevel(logging.ERROR)
            Prettify({PropertyType.PROVIDER: [PROVIDERS_FILE_PATH]})
            log.setLevel(log_level)
        except OSError as e:
            print(
                f"The added provider could not be moved to the correct position "
                f"in '{PROVIDERS_FILE_PATH}': {e}"
            )
            return

        except SystemExit as e:
            print(
                f"The added provider could not be moved to the correct position "
                f"in '{PROVIDERS_FILE_PATH}': {e}"
            )
            return

        print(f"Provider '{domain}' has been added.")
        print("Make sure to add property values by running the bots")

    def _rename_provider(self, old_domain: str, new_domain: str) -> None:
        """Renames a provider by its domain.

        Parameters
        ----------
        old_domain : str
            domain of the provider to be renamed
        new_domain : str
            new domain for the provider to be renamed
        """

        self._show_title_bar()
        print("Action: Rename provider\n")

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        providers = cast(dict[str, ProviderDetailsT], json_data)

        try:
            providers[new_domain] = providers.pop(old_domain)
        except KeyError:
            print(f"Could not rename {old_domain}")
            return

        providers = dict(sorted(providers.items()))

        with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
            formatted_json_string = convert_python_data_to_json_string(providers)
            providers_file.write(formatted_json_string)

        print(f"Provider '{old_domain}' has been renamed to '{new_domain}'.")

    def _delete_provider(self, domain: str) -> None:
        """Deletes a provider by its domain.

        Parameters
        ----------
        domain : str
            domain of the provider to be deleted
        """

        self._show_title_bar()
        print("Action: Delete provider\n")

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        providers = cast(dict[str, ProviderDetailsT], json_data)

        try:
            providers.pop(domain)  # pyright: ignore [reportArgumentType]
        except KeyError:
            print(
                f"Could not delete '{domain}' in '{PROVIDERS_FILE_PATH}' because the "
                f"provider does not exist in that file"
            )
            return

        with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
            formatted_json_string = convert_python_data_to_json_string(providers)
            providers_file.write(formatted_json_string)

        print(f"Provider '{domain}' has been deleted.")

    def _show_continue_prompt(self) -> None:
        """Shows a prompt until any key is pressed."""

        input("\nPress any key to continue...")

    def _clear(self) -> None:
        """Clears the terminal's content."""

        call("clear" if os.name == "posix" else "cls", shell=False)  # noqa: S603

    def _get_action(self) -> str:
        """Prompts for a user action (main menu)."""

        self._show_title_bar()
        print("What would you like to do?")
        print("[%s] Add provider" % ACTION_ADD_PROVIDER)
        print("[%s] Rename provider" % ACTION_RENAME_PROVIDER)
        print("[%s] Delete provider" % ACTION_DELETE_PROVIDER)
        print("[%s] Quit" % ACTION_QUIT)

        return input("Action: ")

    def _get_new_provider_domain_for_addition(self) -> str:
        """Prompts for a new provider's domain.

        Returns
        -------
        str
            domain of the new provider
        """

        name = ""

        while not name:
            self._show_title_bar()
            print("Action: Add provider\n")
            name = input("Provider: ")

        return name

    def _get_provider_domain_for_renaming(self) -> str:
        """Prompts for choosing an existing provider to be renamed.

        Returns
        -------
        str
            domain of the provider to be renamed
        """

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return ""

        providers = cast(dict[str, ProviderDetailsT], json_data)
        possible_positions = ""

        for i, provider in enumerate(providers):
            possible_positions += f"[{i}] {provider}\n"

        while True:
            self._show_title_bar()
            print("Action: Rename provider\n")
            print("Which provider should be renamed?\n")

            print(possible_positions)

            try:
                position = int(input("Input (number in braces): "))

                if position in range(len(providers)):
                    return dict(enumerate(providers))[position]

            except ValueError:
                pass

    def _get_new_provider_domain_for_renaming(self, old_domain: str) -> str:
        """Prompts for a provider's new domain.

        Parameters
        ----------
        old_domain : str
            old domain of the provider

        Returns
        -------
        str
            new domain of the provider
        """

        name = ""

        while not name:
            self._show_title_bar()
            print(f"Action: Rename provider '{old_domain}'\n")
            name = input("New provider name: ")

        return name

    def _get_provider_domain_for_removal(self) -> str:
        """Prompts for choosing an existing provider to be deleted.

        Returns
        -------
        str
            domain of the provider to be deleted
        """

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return ""

        providers = cast(dict[str, ProviderDetailsT], json_data)
        possible_positions = ""

        for i, provider in enumerate(providers):
            possible_positions += f"[{i}] {provider}\n"

        while True:
            self._show_title_bar()
            print("Action: Delete provider\n")
            print("Which provider should be deleted?\n")
            print(possible_positions)

            try:
                position = int(input("Input (number in braces): "))

                if position in range(len(providers)):
                    return dict(enumerate(providers))[position]

            except ValueError:
                pass

    def _show_title_bar(self) -> None:
        """Shows the programm's title bar."""

        self._clear()
        print(50 * "=")
        print("Manager for providers")
        print(50 * "=")
