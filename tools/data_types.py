#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Daniel Brötzmann <daniel.broetzmann@posteo.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains types used for type annotations."""

from __future__ import annotations

from typing import TypedDict


class ProviderDetailsT(TypedDict):
    """Type for providers file: dict[str, ProviderDetailsT]"""

    latestChange: LatestChangeDetailsT
    website: DictStrDetailsT
    alternativeJids: ListStrDetailsT
    busFactor: IntDetailsT
    organization: StringDetailsT
    passwordReset: DictStrDetailsT
    serverTesting: BoolDetailsT
    serverSoftwareName: StringDetailsT
    serverSoftwareVersion: StringDetailsT
    ratingGreenWebCheck: BoolDetailsT
    inBandRegistration: BoolDetailsT
    inBandRegistrationEmailAddressRequired: BoolDetailsT
    inBandRegistrationCaptchaRequired: BoolDetailsT
    registrationWebPage: DictStrDetailsT
    ratingXmppComplianceTester: IntDetailsT
    ratingImObservatoryClientToServer: StringDetailsT
    ratingImObservatoryServerToServer: StringDetailsT
    maximumHttpFileUploadFileSize: IntDetailsT
    maximumHttpFileUploadTotalSize: IntDetailsT
    maximumHttpFileUploadStorageTime: IntDetailsT
    maximumMessageArchiveManagementStorageTime: IntDetailsT
    professionalHosting: BoolDetailsT
    freeOfCharge: BoolDetailsT
    legalNotice: DictStrDetailsT
    serverLocations: ListStrDetailsT
    emailSupport: DictListStrDetailsT
    chatSupport: DictListStrDetailsT
    groupChatSupport: DictListStrDetailsT
    since: StringDetailsT


class StringDetailsT(TypedDict):
    """ProviderDetailsT for str content"""

    content: str
    source: str
    comment: str


class IntDetailsT(TypedDict):
    """ProviderDetailsT for int content"""

    content: int
    source: str
    comment: str


class BoolDetailsT(TypedDict):
    """ProviderDetailsT for bool content"""

    content: bool
    source: str
    comment: str


class ListStrDetailsT(TypedDict):
    """ProviderDetailsT for list[str] content"""

    content: list[str]
    source: str
    comment: str


class DictStrDetailsT(TypedDict):
    """ProviderDetailsT for dict[str, str] content"""

    content: dict[str, str]
    source: str
    comment: str


class DictListStrDetailsT(TypedDict):
    """ProviderDetailsT for dict[str, list[str]] content"""

    content: dict[str, list[str]]
    source: str
    comment: str


class LatestChangeDetailsT(TypedDict):
    """ProviderDetailsT for latestChange key"""

    content: str
    comment: str


AllProviderDetailsT = (
    StringDetailsT
    | IntDetailsT
    | BoolDetailsT
    | ListStrDetailsT
    | DictStrDetailsT
    | DictListStrDetailsT
    | LatestChangeDetailsT
)


class FilteredDetailsT(TypedDict):
    """Type for filtered providers (as found in filtered provider lists)"""

    jid: str
    category: str
    latestChange: str
    website: dict[str, str]
    alternativeJids: list[str]
    busFactor: int
    organization: str
    passwordReset: dict[str, str]
    serverTesting: bool
    serverSoftwareName: str
    serverSoftwareVersion: str
    ratingGreenWebCheck: bool
    inBandRegistration: bool
    inBandRegistrationEmailAddressRequired: bool
    inBandRegistrationCaptchaRequired: bool
    registrationWebPage: dict[str, str]
    ratingXmppComplianceTester: int
    ratingImObservatoryClientToServer: str
    ratingImObservatoryServerToServer: str
    maximumHttpFileUploadFileSize: int
    maximumHttpFileUploadTotalSize: int
    maximumHttpFileUploadStorageTime: int
    maximumMessageArchiveManagementStorageTime: int
    professionalHosting: bool
    freeOfCharge: bool
    legalNotice: dict[str, str]
    serverLocations: list[str]
    emailSupport: dict[str, list[str]]
    chatSupport: dict[str, list[str]]
    groupChatSupport: dict[str, list[str]]
    since: str
