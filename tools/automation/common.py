#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains code needed by various automation tools."""

from __future__ import annotations

from typing import Any
from typing import TYPE_CHECKING

import logging

from tools.common import convert_python_data_to_json_string
from tools.common import load_json_file
from tools.common import PROVIDERS_FILE_PATH

if TYPE_CHECKING:
    from tools.data_types import ProviderDetailsT

log = logging.getLogger()


def update_property(
    provider: str,
    properties: ProviderDetailsT,
    property_name: str,
    new_property_content: Any,
    new_property_source: str,
) -> None:
    """Updates a provider property in case of changes.

    Parameters
    ----------
    provider : str
        provider whose property is to be updated
    properties : ProviderDetailsT
        properties to be updated
    property_name : str
        property to be checked
    new_property_content : Any
        latest value of the property's content field or None on error
    new_property_source : str
        latest value of the property's source field
    """

    if new_property_content is None:
        log.debug(
            "%s: Skipping further processing of property '%s' since data could not be "
            "retrieved",
            provider,
            property_name,
        )
        return

    property_data = properties[property_name]

    current_property_comment = property_data.get("comment", None)

    if current_property_comment is not None and current_property_comment:
        log.debug(
            "%s: Skipping further processing of property '%s' "
            "since non-empty 'comment' prevents automatic updates",
            provider,
            property_name,
        )
        return

    current_property_content = property_data["content"]

    if new_property_content == current_property_content:
        log.debug(
            "%s: 'content' of '%s' is still '%s'",
            provider,
            property_name,
            current_property_content,
        )
    else:
        log.info(
            "%s: 'content' of '%s' changed from '%s' to '%s'",
            provider,
            property_name,
            current_property_content,
            new_property_content,
        )

        property_data["content"] = new_property_content

    current_property_source = property_data.get("source", "")

    if new_property_source != current_property_source:
        log.info(
            "%s: 'source' of '%s' changed from '%s' to '%s'",
            provider,
            property_name,
            current_property_source,
            new_property_source,
        )

        property_data["source"] = new_property_source

    if current_property_comment is not None:
        property_data.pop("comment")
        log.info(
            "%s: 'comment' of '%s' removed since it is empty",
            provider,
            property_name,
        )


def update_providers_file(provider_data: dict[str, ProviderDetailsT]) -> None:
    """Updates the providers file.

    Parameters
    ----------
    provider_data : dict[str, ProviderDetailsT]
        data of all providers to replace the current data of the providers file
    """

    if provider_data == load_json_file(PROVIDERS_FILE_PATH):
        log.debug(
            "Not updating %s because no changes were detected", PROVIDERS_FILE_PATH
        )
        return

    with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
        providers_file.write(convert_python_data_to_json_string(provider_data))
        log.debug("Updated %s", PROVIDERS_FILE_PATH)
