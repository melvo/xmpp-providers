#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
# SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains an HTTP/REST bot for provider ratings."""

from __future__ import annotations

from typing import Any
from typing import cast

import asyncio
import json
import logging
import random
from collections.abc import Awaitable
from collections.abc import Callable
from collections.abc import Iterable
from http import HTTPStatus
from urllib.parse import urlparse

from aiohttp import ClientError
from aiohttp import ClientResponse
from aiohttp import ClientSession
from aiohttp import ClientTimeout
from aiohttp import ContentTypeError
from bs4 import BeautifulSoup
from dns.rdtypes.IN.SRV import SRV
from dns.resolver import LifetimeTimeout
from dns.resolver import NoAnswer
from dns.resolver import NoNameservers
from dns.resolver import NXDOMAIN
from dns.resolver import resolve
from jsonschema import Draft202012Validator
from jsonschema import ValidationError

from tools.automation.common import update_property
from tools.automation.common import update_providers_file
from tools.common import load_json_file
from tools.common import PROVIDER_FILE_SCHEMA_FILE_PATH
from tools.common import PROVIDERS_FILE_PATH
from tools.data_types import ProviderDetailsT

log = logging.getLogger()

RATING_XMPP_COMPLIANCE_TESTER_KEY = "ratingXmppComplianceTester"
RATING_GREEN_WEB_CHECK_KEY = "ratingGreenWebCheck"
URL_PROPERTIES = ("website", "passwordReset", "legalNotice")

PROVIDER_FILE_URL = "https://%s/.well-known/xmpp-provider-v2.json"

XMPP_COMPLIANCE_TESTER_URL = "https://compliance.conversations.im/server/%s/"
XMPP_COMPLIANCE_TESTER_UPDATE_URL = "https://compliance.conversations.im/live/%s/"
GREEN_WEB_CHECK_URL = "https://api.thegreenwebfoundation.org/api/v3/greencheck/%s"

REQUEST_TIMEOUT_SECONDS = 10
MIN_UPDATE_TIMEOUT_SECONDS = 5
MAX_UPDATE_TIMEOUT_SECONDS = 10


class WebBot:
    """Queries or updates ratings for providers in the providers file and writes
    changes to it.

    Parameters
    ----------
    update : bool
        whether to trigger rating updates for providers instead of querying the
        current rating
    providers: list[str]
        domains of providers to be checked (or whose ratings are to be updated) or None
        for checking all providers (default: all providers are checked and their ratings
        updated)
    """

    def __init__(
        self, update: bool = False, providers: list[str] | None = None
    ) -> None:
        log.info("Starting provider rating check")

        self._provider_file_schema = load_json_file(PROVIDER_FILE_SCHEMA_FILE_PATH)

        if self._provider_file_schema is None:
            return

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        self._provider_data = cast(dict[str, ProviderDetailsT], json_data)

        if providers is None:
            self._providers = list(self._provider_data.keys())
        else:
            self._providers = providers

        asyncio.run(self._run(update))

    async def _run(self, update: bool) -> None:
        """Runs all actions.

        Parameters
        ----------
        update : bool
            whether to trigger rating updates for providers instead of querying the
            current rating
        """

        # "raise_for_status=True" is needed to raise an aiohttp.ClientResponseError
        # after REQUEST_TIMEOUT_SECONDS seconds if the response status code is 400 or
        # higher in order to log an error.
        async with ClientSession(
            timeout=ClientTimeout(total=REQUEST_TIMEOUT_SECONDS),
            raise_for_status=True,
        ) as session:
            if update:
                await self._trigger_updates(session)
            else:
                await self._update_providers(session)

    async def _update_providers(self, session: ClientSession) -> None:
        """Checks the properties of all providers in the providers file for changes and
        writes the changes to the providers file.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        """

        async with asyncio.TaskGroup() as task_group:
            task_group.create_task(
                self._check_provider_ratings_from_all_rating_services(session)
            )

            for provider in self._providers:
                task_group.create_task(self._check_provider_file(session, provider))

        update_providers_file(self._provider_data)

    async def _check_provider_ratings_from_all_rating_services(
        self, session: ClientSession
    ) -> None:
        """Retrieves the ratings of all providers from all rating services, checks "
        them for changes and updates the providers' cached data.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        """

        property_updates: dict[
            str, Callable[[ClientSession, str], Awaitable[tuple[Any, str]]]
        ] = {
            RATING_XMPP_COMPLIANCE_TESTER_KEY: self._get_xmpp_compliance_tester_rating,
            RATING_GREEN_WEB_CHECK_KEY: self._get_green_web_check_rating,
        }

        async with asyncio.TaskGroup() as task_group:
            for property_name, check_property in property_updates.items():
                task_group.create_task(
                    self._check_provider_ratings(session, property_name, check_property)
                )

    async def _check_provider_ratings(
        self,
        session: ClientSession,
        property_name: str,
        check_property: Callable[[ClientSession, str], Awaitable[tuple[Any, str]]],
    ) -> None:
        """Retrieves the ratings of all providers from a rating service, checks them "
        "for changes and updates the providers' cached data.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        property_name : str
            name of the property to be checked
        check_property : Callable[[ClientSession, str], Awaitable[tuple[Any, str]]]
            method checking the property
        """

        for provider in self._providers:
            properties = self._provider_data.get(provider)

            if properties is None:
                log.warning(
                    "%s: Skipping check for property '%s' because provider is not in "
                    "providers file",
                    provider,
                    property_name,
                )
                return

            new_property_content, new_property_source = await check_property(
                session, provider
            )

            update_property(
                provider,
                properties,
                property_name,
                new_property_content,
                new_property_source,
            )

    async def _check_provider_file(self, session: ClientSession, provider: str) -> None:
        """Retrieves the properties a provider supplies via its provider file, checks
        them for changes and updates the provider's cached data.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        provider : str
            provider whose provider file is to be retrieved
        """

        properties = self._provider_data.get(provider)

        if properties is None:
            log.warning(
                "%s: Skipping provider file check because provider is not in providers "
                "file",
                provider,
            )
            return

        (
            provider_file_data,
            new_property_source,
        ) = await self._get_provider_file_data(session, provider)

        if provider_file_data is None:
            log.warning("%s: Could not retrieve provider file data", provider)
            return

        for (
            property_name,
            new_property_content,
        ) in provider_file_data.items():
            update_property(
                provider,
                properties,
                property_name,
                new_property_content,
                new_property_source,
            )

    async def _get_xmpp_compliance_tester_rating(
        self, session: ClientSession, provider: str
    ) -> tuple[int | None, str]:
        """Queries the XMPP Compliance Tester rating.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        provider : str
            provider whose rating is to be retrieved

        Returns
        -------
        tuple[int | None, str]
            provider rating (or None on error) and its source
        """

        url = XMPP_COMPLIANCE_TESTER_URL % provider
        response = await self._request_url(session, url)

        if response is None:
            return None, url

        soup = BeautifulSoup(await response.text(), "html.parser")
        result = soup.find("div", class_="stat_result")

        if result is None:
            return None, url

        return int(result.text.strip()[:-1]), url

    async def _get_green_web_check_rating(
        self, session: ClientSession, provider: str
    ) -> tuple[bool | None, str]:
        """Queries the Green Web Check rating.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        provider : str
            provider whose rating is to be retrieved

        Returns
        -------
        tuple[bool | None, str]
            whether the provider is rated as being green (or None on error) and its
            source
        """

        url = GREEN_WEB_CHECK_URL % self._get_xmpp_srv_host(provider)
        response = await self._request_url(session, url)

        if response is None:
            return None, url

        json_data = await self._extract_json_data(response)

        if json_data is None:
            return None, url

        return json_data.get("green"), url

    @staticmethod
    def _get_xmpp_srv_host(provider: str) -> str:
        """Tries to resolve both the "_xmpp-client._tcp"
        (see https://datatracker.ietf.org/doc/html/rfc6120#section-3.2.1) and
        "_xmpps-client._tcp" (see https://xmpp.org/extensions/xep-0368.html)
        hostnames (i.e., the SRV record's target field) and returns the one with the
        highest priority (i.e., the lowest value).

        Parameters
        ----------
        provider : str
            provider whose SRV records are to be retrieved

        Returns
        -------
        str
            "_xmpp-client" or "_xmpps-client" hostname with the highest priority,
            if available, otherwise the unmodified provider
        """

        queries = [
            f"_xmpps-client._tcp.{provider}",
            f"_xmpp-client._tcp.{provider}",
        ]

        results: dict[int, str] = {}

        for query in queries:
            try:
                answer = resolve(query, "SRV")
            except (LifetimeTimeout, NoAnswer, NoNameservers, NXDOMAIN) as e:
                log.warning("Could not retrieve XMPP SRV host from '%s': %s", query, e)
                continue

            if answer.rrset is None:
                continue

            for record in cast(list[SRV], answer):
                host = record.target.to_text(omit_final_dot=True)
                log.debug("Retrieved XMPP SRV host from '%s': %s", answer.name, host)
                results[record.priority] = host

        if results:
            highest_priority = min(results.keys())
            return results[highest_priority]

        return provider

    async def _trigger_updates(self, session: ClientSession) -> None:
        """Triggers updates for provider ratings at services supporting updates.

        Currently supported:
        - XMPP Compliance Tester

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        """

        log.info("Triggering provider rating updates")

        provider_count = len(self._providers)

        for i, provider in enumerate(self._providers):
            await self._update_xmpp_compliance_tester_rating(session, provider)

            if i + 1 < provider_count:
                timeout_seconds = random.randint(  # noqa: S311
                    MIN_UPDATE_TIMEOUT_SECONDS, MAX_UPDATE_TIMEOUT_SECONDS
                )

                log.debug(
                    "Waiting for %s seconds to trigger the next rating update",
                    timeout_seconds,
                )

                # Add timeout to avoid overloading the rating service.
                # The timeout is randomly chosen to avoid multiple bots querying the
                # same service with similar intervals.
                await asyncio.sleep(timeout_seconds)

    async def _update_xmpp_compliance_tester_rating(
        self, session: ClientSession, provider: str
    ) -> None:
        """Triggers an XMPP Compliance Tester check for updating a provider rating.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        provider : str
            provider domain
        """

        log.info(
            "%s: Updating result for property '%s'",
            provider,
            RATING_XMPP_COMPLIANCE_TESTER_KEY,
        )

        url = XMPP_COMPLIANCE_TESTER_UPDATE_URL % provider

        if await self._request_url(session, url):
            log.debug(
                "%s: Updated result for property '%s'",
                provider,
                RATING_XMPP_COMPLIANCE_TESTER_KEY,
            )
        else:
            log.warning(
                "%s: Could not update result for property '%s'",
                provider,
                RATING_XMPP_COMPLIANCE_TESTER_KEY,
            )

    async def _get_provider_file_data(
        self,
        session: ClientSession,
        provider: str,
    ) -> tuple[dict[str, Any] | None, str]:
        """Queries properties stored in a provider file (i.e., a JSON file on the
        provider's web server for properties that cannot be queried via other methods).

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        provider : str
            provider whose provider file is retrieved

        Returns
        -------
        tuple[dict[str, Any] | None, str]
            property names mapped to their retrieved content (or None on error) and its
            source
        """

        url = PROVIDER_FILE_URL % provider
        log.debug("%s: Requesting provider file %s", provider, url)

        response = await self._request_url(session, url, True)

        if response is None:
            return None, url

        provider_file_properties = await self._validate_provider_file(response)

        if provider_file_properties is None:
            return None, url

        property_content_fields = {}

        for property_name, property_content in provider_file_properties.items():
            # Remove a trailing slash from a URL without a path to retain a consistent
            # format.
            if property_name in URL_PROPERTIES:
                for language_code, web_page_url in property_content.items():
                    url_parts = urlparse(web_page_url)

                    if (
                        url_parts.path == "/"
                        and not url_parts.query
                        and not url_parts.fragment
                    ):
                        property_content[language_code] = (
                            f"{url_parts.scheme}://{url_parts.hostname}"
                        )

            # Remove the main JID from the alternative JIDs.
            # That is needed because some provider files wrongly list the main JID as an
            # alternative JID.
            elif property_name == "alternativeJids" and provider in property_content:
                property_content.remove(provider)

            # Skip an empty "since" property to keep the currently stored date.
            # That remains the date of the initial provider addition if no date since
            # that the provider is available is set in the provider file.
            elif property_name == "since" and property_content == "":
                continue

            property_content_fields[property_name] = property_content

        if not property_content_fields:
            return None, url

        log.debug(
            "%s: Extracted valid property content: %s",
            provider,
            property_content_fields,
        )
        return property_content_fields, url

    async def _validate_provider_file(self, response: ClientResponse) -> dict | None:
        """Parses a provider file from a response and validates its JSON data.

        Parameters
        ----------
        response : aiohttp.ClientResponse
            response containing the provider file JSON data

        Returns
        -------
        dict | None
            valid property names mapped to their content or None on error
        """

        url = response.url
        log.debug("Validating %s", url)

        json_data = await self._extract_json_data(response)

        if json_data is None:
            log.warning("Could not parse provider file JSON data from %s", url)
            return None

        provider_file_data = json_data
        log.debug("Parsed provider file JSON data from %s: %s", url, provider_file_data)

        validator = Draft202012Validator(
            self._provider_file_schema,
            format_checker=Draft202012Validator.FORMAT_CHECKER,
        )

        validation_errors = cast(
            Iterable[ValidationError], validator.iter_errors(provider_file_data)
        )

        # Remove invalid properties that are defined in PROVIDER_FILE_SCHEMA_FILE_PATH.
        for validation_error in validation_errors:
            invalid_property_path = validation_error.relative_path

            if len(invalid_property_path):
                invalid_property = invalid_property_path[0]
                log.warning(
                    "Ignoring property '%s' of provider file %s because it does not "
                    "match schema '%s': %s",
                    invalid_property,
                    url,
                    PROVIDER_FILE_SCHEMA_FILE_PATH,
                    validation_error.message,
                )

                # "pop()" with None is used here instead of "del" to skip an already
                # removed property and avoid a KeyError.
                # That is the case for a list or dict property with at least two items
                # not matching the schema because the first invalid item triggers the
                # property's removal and all other invalid items would trigger the same.
                provider_file_data.pop(invalid_property, None)

        # Remove properties that are not defined in PROVIDER_FILE_SCHEMA_FILE_PATH.
        for property_name in list(provider_file_data.keys()):
            if property_name not in self._provider_file_schema["properties"]:
                log.warning(
                    "Ignoring property '%s' of provider file %s because it is not "
                    "defined in schema '%s'",
                    property_name,
                    url,
                    PROVIDER_FILE_SCHEMA_FILE_PATH,
                )
                del provider_file_data[property_name]

        return provider_file_data

    @staticmethod
    async def _request_url(
        session: ClientSession, url: str, redirects_allowed: bool = False
    ) -> ClientResponse | None:
        """Requests a URL.

        Parameters
        ----------
        session : aiohttp.ClientSession
            session used for the request
        url : str
            URL to be retrieved
        redirects_allowed : bool
            whether redirects are allowed (default: False)

        Returns
        -------
        aiohttp.ClientResponse | None
            response or None on error
        """

        log.debug("Requesting %s", url)

        try:
            response = await session.get(url, allow_redirects=redirects_allowed)

        except TimeoutError:
            log.warning("Requesting %s failed because of a timeout", url)
            return None

        except ClientError as e:
            log.warning("Requesting %s failed: %s", url, e)
            return None

        else:
            status_code = response.status

            if status_code != HTTPStatus.OK:
                log.warning(
                    "Requesting %s failed with status code %s", url, status_code
                )
                return None

            return response

        return None

    @staticmethod
    async def _extract_json_data(response: ClientResponse) -> Any:
        """Extracts JSON data from a response.

        Parameters
        ----------
        response : aiohttp.ClientResponse
            response with JSON data

        Returns
        -------
        Any
            JSON data or None on error
        """

        try:
            return await response.json()
        except ContentTypeError as e:
            log.warning("%s is not interpreted as JSON data: %s", response.url, e)
            return None
        except json.JSONDecodeError as e:
            log.exception(
                "%s could not be parsed because of invalid JSON syntax: %s in line %s "
                "at column %s",
                response.url,
                e.msg,
                e.lineno,
                e.colno,
            )
            return None
