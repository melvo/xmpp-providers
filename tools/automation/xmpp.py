#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains an XMPP bot for fetching properties from the providers' servers."""

from __future__ import annotations

from typing import Any
from typing import cast
from typing import Literal
from typing import TYPE_CHECKING

import asyncio
import logging
import secrets
import string
from collections import defaultdict
from errno import EADDRNOTAVAIL
from errno import EAFNOSUPPORT

from slixmpp.clientxmpp import ClientXMPP
from slixmpp.exceptions import IqError
from slixmpp.exceptions import IqTimeout
from slixmpp.jid import JID
from slixmpp.plugins.xep_0030.disco import XEP_0030
from slixmpp.plugins.xep_0077.register import XEP_0077
from slixmpp.plugins.xep_0092.version import XEP_0092

from tools.automation.common import update_property
from tools.automation.common import update_providers_file
from tools.common import load_json_file
from tools.common import PROVIDERS_FILE_PATH
from tools.data_types import ProviderDetailsT

if TYPE_CHECKING:
    from collections.abc import Awaitable
    from collections.abc import Callable

    from slixmpp.features.feature_mechanisms.stanza.failure import Failure
    from slixmpp.stanza import Iq

log = logging.getLogger()

INITIAL_REQUEST_TIMEOUT_SECONDS = 120
REQUEST_TIMEOUT_SECONDS = 10

RANDOM_USERNAME_LENGTH = 20
RANDOM_USERNAME_CHARACTERS = string.ascii_lowercase + string.digits

RANDOM_PASSWORD_LENGTH = 20
RANDOM_PASSWORD_CHARACTERS = (
    string.ascii_lowercase + string.ascii_uppercase + string.digits
)

SPLIT_STRING_PARTS_COUNT = 2
MIN_SPLIT_STRING_PARTS_COUNT = 2

HTTP_FILE_UPLOAD_NAMESPACES = ["urn:xmpp:http:upload:0"]
MAXIMUM_HTTP_FILE_UPLOAD_FILE_SIZE_FORM_FIELD_KEY = "max-file-size"

IBR_KEY = "inBandRegistration"
IBR_EMAIL_ADDRESS_REQUIRED_KEY = "inBandRegistrationEmailAddressRequired"
IBR_CAPTCHA_REQUIRED_KEY = "inBandRegistrationCaptchaRequired"
REGISTRATION_WEB_PAGE_KEY = "registrationWebPage"
MAX_UPLOAD_SIZE_KEY = "maximumHttpFileUploadFileSize"
SERVER_SOFTWARE_NAME_KEY = "serverSoftwareName"
SERVER_SOFTWARE_VERSION_KEY = "serverSoftwareVersion"
EMAIL_SUPPORT_KEY = "emailSupport"
CHAT_SUPPORT_KEY = "chatSupport"
GROUP_CHAT_SUPPORT_KEY = "groupChatSupport"

IBR_URL = "https://xmpp.org/extensions/xep-0077.html"
CAPTCHA_URL = "https://xmpp.org/extensions/xep-0158.html"
HTTP_FILE_UPLOAD_URL = "https://xmpp.org/extensions/xep-0363.html"
SOFTWARE_VERSION_URL = "https://xmpp.org/extensions/xep-0092.html"
CONTACT_ADDRESSES_URL = "https://xmpp.org/extensions/xep-0157.html"

DATA_FORM_FIELD_ATTRIBUTE_KEY = "var"
DATA_FORM_FIELD_VALUE_START_TAG = "<value>"
DATA_FORM_FIELD_VALUE_END_TAG = "</value>"

REGISTRATION_USERNAME_KEY = "username"
REGISTRATION_PASSWORD_KEY = "password"  # noqa: S105
REGISTRATION_EMAIL_ADDRESS_KEY = "email"
# See https://xmpp.org/extensions/xep-0158.html#table-1
REGISTRATION_CAPTCHA_KEYS = [
    "audio_recog",
    "ocr",
    "picture_q",
    "picture_recog",
    "qa",
    "speech_q",
    "speech_recog",
    "video_q",
    "video_recog",
]
CAPTCHA_STRING = "captcha"

SOFTWARE_INFO_KEY = "software_version"
SOFTWARE_INFO_NAME_KEY = "name"
SOFTWARE_INFO_VERSION_KEY = "version"

ContactAddressKeyT = Literal["emailSupport", "chatSupport", "groupChatSupport"]
XMPP_URI_SCHEME = "xmpp"
XMPP_URI_GROUP_CHAT_QUERY_TYPE = "join"
EMAIL_URI_SCHEME = "mailto"


class XmppBot:
    """Queries properties for providers in the providers file and writes changes to it.

    Parameters
    ----------
    jid : str
        bare JID of the XMPP account used to communicate with the providers' servers
    password : str
        password for the JID
    providers: list[str]
        domains of providers to be checked or None for checking all providers (default:
        all providers are checked)
    """

    def __init__(
        self, jid: str, password: str, providers: list[str] | None = None
    ) -> None:
        log.info("Starting XMPP bot")

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        self._jid = jid
        self._password = password
        self._provider_data = cast(dict[str, ProviderDetailsT], json_data)

        if providers is None:
            self._providers = list(self._provider_data.keys())
        else:
            self._providers = providers

        asyncio.run(self._update_providers())

    async def _update_providers(self) -> None:
        """Checks the properties of all providers in the providers file for changes and
        writes the changes to the providers file.
        """

        async with asyncio.TaskGroup() as task_group:
            task_group.create_task(self._check_regular_properties())
            task_group.create_task(self._check_registration_support())

        update_providers_file(self._provider_data)

    async def _check_regular_properties(self) -> None:
        """Checks all properties that are retrieved via an existing XMPP account."""

        await XmppClient(
            self._jid, self._password, self._providers, self._provider_data
        ).disconnected

    async def _check_registration_support(self) -> None:
        """Checks the registration support via unauthenticated sessions."""

        async with asyncio.TaskGroup() as task_group:
            for provider in self._providers:
                task_group.create_task(
                    self._check_provider_registration_support(provider)
                )

    async def _check_provider_registration_support(self, provider: str) -> None:
        """Checks a provider's registration support via unauthenticated sessions.

        Parameters
        ----------
        provider : str
            provider whose registration support is to be checked
        """

        await XmppRegistrationClient(provider, self._provider_data).disconnected


class XmppClient(ClientXMPP):
    """Queries properties of providers in the providers file.

    Communication flow: XMPP bot <-> XMPP bot server <-> provider server

    Parameters
    ----------
    jid : str
        bare JID of the XMPP account used to communicate with the providers' servers
    password : str
        password for the JID
    providers: list[str]
        domains of providers to be checked
    provider_data : dict[str, ProviderDetailsT]
        data of all providers to be updated
    """

    def __init__(
        self,
        jid: str,
        password: str,
        providers: list[str],
        provider_data: dict[str, ProviderDetailsT],
    ) -> None:
        log.info("Starting regular properties check for all providers")

        ClientXMPP.__init__(self, jid, password)

        self._providers = providers
        self._provider_data = provider_data
        self._disco_info: dict[str, dict[str, Any]] = defaultdict(
            lambda: defaultdict(str)
        )
        self._server_software: dict[str, dict[str, str]] = defaultdict(
            lambda: defaultdict(str)
        )

        self.add_event_handler("session_start", self._update_providers)
        self.add_event_handler("failed_all_auth", self._handle_failed_all_auth)

        self.connect()

    def _handle_failed_all_auth(self, _event_data: Any) -> None:
        """Handles the case that all login attempts failed.

        Parameters
        ----------
        _event_data : Any
            data provided by the event that triggers this method
        """

        log.error(
            "Could not log in with '%s' to check regular properties", self.boundjid.bare
        )

    async def _update_providers(self, _event_data: Any) -> None:
        """Checks the provider properties for changes and updates the provider data.

        Parameters
        ----------
        _event_data : Any
            data provided by the event that triggers this method
        """

        async with asyncio.TaskGroup() as task_group:
            for provider in self._providers:
                task_group.create_task(self._update_provider(provider))

        self.disconnect()

    async def _update_provider(self, provider: str) -> None:
        """Checks a provider's properties for changes and updates the provider data.

        Parameters
        ----------
        provider : str
            provider whose properties are to be checked
        """

        property_updates: dict[str, Callable[[str], Awaitable[tuple[Any, str]]]] = {
            MAX_UPLOAD_SIZE_KEY: self._get_maximum_http_file_upload_file_size,
            SERVER_SOFTWARE_NAME_KEY: self._get_server_software_name,
            SERVER_SOFTWARE_VERSION_KEY: self._get_server_software_version,
            EMAIL_SUPPORT_KEY: self._get_email_support_addresses,
            CHAT_SUPPORT_KEY: self._get_chat_support_addresses,
            GROUP_CHAT_SUPPORT_KEY: self._get_group_chat_support_addresses,
        }

        properties = self._provider_data.get(provider)

        if properties is None:
            log.warning(
                "%s: Skipping check because provider is not in providers file '%s'",
                provider,
                PROVIDERS_FILE_PATH,
            )

            return

        await self._query_disco_data(provider)

        for property_name, check_property in property_updates.items():
            new_property_content, new_property_source = await check_property(provider)

            update_property(
                provider,
                properties,
                property_name,
                new_property_content,
                new_property_source,
            )

    async def _query_disco_data(self, provider: str) -> None:
        """Queries Service Discovery (XEP-0030) data for a provider and caches it to be
        used later by various checks.

        Parameters
        ----------
        provider : str
            provider whose Service Discovery data is to be queried
        """

        await self._query_disco_info(
            provider, provider, INITIAL_REQUEST_TIMEOUT_SECONDS
        )

        disco_items = await self._query_disco_items(provider)

        if disco_items is not None:
            for disco_item in disco_items:
                disco_item_jid = disco_item[0]
                await self._query_disco_info(provider, disco_item_jid)

    async def _query_disco_info(
        self,
        provider: str,
        item_jid: str,
        request_timeout: int = REQUEST_TIMEOUT_SECONDS,
    ) -> None:
        """Queries Service Discovery (XEP-0030) info for an item and caches it to be
        used later by various checks.

        Parameters
        ----------
        provider : str
            provider whose Service Discovery info is to be queried
        item_jid : str
            JID of the Service Discovery item whose info is to be queried
        request_timeout : int
            seconds to wait for the server's response (default: REQUEST_TIMEOUT_SECONDS)
        """

        self.register_plugin("xep_0030")

        disco_info_iq = None

        try:
            disco_info_iq = await cast(XEP_0030, self["xep_0030"]).get_info(
                JID(item_jid), timeout=request_timeout
            )

        except IqError:
            log.warning(
                "%s: Disco item '%s': Could not get service discovery info",
                provider,
                item_jid,
            )

        except IqTimeout:
            log.warning(
                "%s: Disco item '%s': Could not get service discovery info within %s "
                "second/s",
                provider,
                item_jid,
                request_timeout,
            )

        if disco_info_iq is None:
            self._disco_info[provider][item_jid] = None
            return

        disco_info = disco_info_iq["disco_info"]
        disco_features = disco_info["features"]
        log.debug(
            "%s: Disco item '%s': Discovered features: %s",
            provider,
            item_jid,
            disco_features,
        )
        self._disco_info[provider][item_jid] = disco_info

    async def _query_disco_items(self, provider: str) -> list[Any] | None:
        """Queries Service Discovery (XEP-0030) items for a provider.

        Parameters
        ----------
        provider : str
            provider whose Service Discovery items are to be queried

        Returns
        -------
        list[Any] | None
            list of Service Discovery items or None on error
        """

        disco_items_iq = None

        try:
            disco_items_iq = await cast(XEP_0030, self["xep_0030"]).get_items(
                JID(provider), timeout=REQUEST_TIMEOUT_SECONDS
            )

        except IqError:
            log.warning("%s: Could not get service discovery items", provider)

        except IqTimeout:
            log.warning("%s: Could not get service discovery items", provider)

        if disco_items_iq is None:
            return None

        disco_items = disco_items_iq["disco_items"]["items"]
        log.debug("%s: Discovered items: %s", provider, disco_items)

        return disco_items

    async def _get_maximum_http_file_upload_file_size(
        self, provider: str
    ) -> tuple[int | None, str]:
        """Queries the maximum size of an uploaded file.

        Parameters
        ----------
        provider : str
            provider whose property is to be retrieved

        Returns
        -------
        tuple[int | None, str]
            maximum size of an uploaded file (or None on error) and its source
        """

        http_upload_file_size = None

        for disco_info in self._disco_info[provider].values():
            if disco_info is None:
                continue

            for disco_feature in disco_info["features"]:
                if disco_feature not in HTTP_FILE_UPLOAD_NAMESPACES:
                    continue

                disco_info_parts = f"{disco_info}".split(
                    MAXIMUM_HTTP_FILE_UPLOAD_FILE_SIZE_FORM_FIELD_KEY
                )

                if len(disco_info_parts) < MIN_SPLIT_STRING_PARTS_COUNT:
                    http_upload_file_size = 0
                    break

                form_parts = disco_info_parts[1].split(DATA_FORM_FIELD_VALUE_START_TAG)

                if len(form_parts) < MIN_SPLIT_STRING_PARTS_COUNT:
                    http_upload_file_size = 0
                    break

                upload_size_bytes = int(
                    form_parts[1].split(DATA_FORM_FIELD_VALUE_END_TAG)[0]
                )
                upload_size_megabytes = int(upload_size_bytes / 1000000)

                if upload_size_megabytes < 1:
                    http_upload_file_size = -1
                    break

                http_upload_file_size = upload_size_megabytes
                break

        return http_upload_file_size, HTTP_FILE_UPLOAD_URL

    async def _get_email_support_addresses(
        self, provider: str
    ) -> tuple[dict[str, list[str]], str]:
        """Searches email support addresses within the cached Dervice Discovery
        information.

        Parameters
        ----------
        provider : str
            provider whose contact address is to be searched

        Returns
        -------
        tuple[dict[str, list[str]], str]
            language codes mapped to email support addresses and their source
        """

        return await self._get_contact_addresses(provider, EMAIL_SUPPORT_KEY)

    async def _get_chat_support_addresses(
        self, provider: str
    ) -> tuple[dict[str, list[str]], str]:
        """Searches chat support addresses within the cached Service Discovery
        information.

        Parameters
        ----------
        provider : str
            provider whose contact address is to be searched

        Returns
        -------
        tuple[dict[str, list[str]], str]
            language codes mapped to chat support addresses and their source
        """

        return await self._get_contact_addresses(provider, CHAT_SUPPORT_KEY)

    async def _get_group_chat_support_addresses(
        self, provider: str
    ) -> tuple[dict[str, list[str]], str]:
        """Searches group chat addresses within the cached Service Discovery
        information.

        Parameters
        ----------
        provider : str
            provider whose contact address is to be searched

        Returns
        -------
        tuple[dict[str, list[str]], str]
            language codes mapped to group chat support addresses and their source
        """

        return await self._get_contact_addresses(provider, GROUP_CHAT_SUPPORT_KEY)

    async def _get_contact_addresses(
        self, provider: str, address_type: ContactAddressKeyT
    ) -> tuple[dict[str, list[str]], str]:
        """Searches contact addresses within the cached Service Discovery information.

        Parameters
        ----------
        provider : str
            provider whose contact address is to be searched
        address_type : ContactAddressKeyT
            type of address

        Returns
        -------
        tuple[dict[str, list[str]], str]
            language codes mapped to addresses and their source
        """

        addresses: dict[str, list[str]] = defaultdict(list)

        disco_info = self._disco_info[provider][provider]

        if disco_info is not None:
            data_elements = disco_info.xml.findall("{jabber:x:data}x")

            for data_element in data_elements:
                serverinfo_element = data_element.find(
                    '{jabber:x:data}field[@var="FORM_TYPE"]/'
                    "{jabber:x:data}value[.='http://jabber.org/network/serverinfo']"
                )

                if serverinfo_element is None:
                    continue

                address_elements = data_element.findall(
                    '{jabber:x:data}field[@var="support-addresses"]/'
                    "{jabber:x:data}value"
                )

                for address_element in address_elements:
                    address = self._extract_contact_address(
                        address_element.text, address_type
                    )

                    if address is not None:
                        addresses["en"].append(address)

                break

        return addresses, CONTACT_ADDRESSES_URL

    @staticmethod
    def _extract_contact_address(
        address_uri: str, address_type: ContactAddressKeyT
    ) -> str | None:
        """Extracts an appropriate contact address from an address URI.

        Parameters
        ----------
        address_uri : str
            address URI
        address_type : SupportKeyT
            type of address

        Returns
        -------
        str | None
            contact address or None if unsure
        """

        log.debug(
            "Found possible contact address URI for type '%s': %s",
            address_type,
            address_uri,
        )

        if address_type == EMAIL_SUPPORT_KEY:
            return XmppClient._extract_address(address_uri, EMAIL_URI_SCHEME)

        if address_type == CHAT_SUPPORT_KEY:
            return XmppClient._extract_address(address_uri, XMPP_URI_SCHEME)

        if address_type == GROUP_CHAT_SUPPORT_KEY:
            return XmppClient._extract_address(
                address_uri, XMPP_URI_SCHEME, XMPP_URI_GROUP_CHAT_QUERY_TYPE
            )

        return None

    @staticmethod
    def _extract_address(
        address_uri: str, uri_scheme: str, uri_query_type: str | None = None
    ) -> str | None:
        """Extracts an address from an address URI.

        Parameters
        ----------
        address_uri : str
            address URI
        uri_scheme : str
            scheme of the URI
        uri_query_type : str | None
            query type of the URI or None if no query type used (default: None)

        Returns
        -------
        str | None
            contact address or None if not found
        """

        parts = address_uri.split(":")

        if len(parts) != SPLIT_STRING_PARTS_COUNT or parts[0] != uri_scheme:
            return None

        address_part = parts[1]

        parts = address_part.split("?")

        if uri_query_type is None:
            return address_part if len(parts) == 1 else None

        if len(parts) != SPLIT_STRING_PARTS_COUNT or parts[1] != uri_query_type:
            return None

        return parts[0]

    async def _get_server_software_name(self, provider: str) -> tuple[str | None, str]:
        """Queries the server software name.

        Parameters
        ----------
        provider : str
            provider whose property is to be retrieved

        Returns
        -------
        tuple[str | None, str]
            software name (or None on error) and its source
        """

        name, _version, source = await self._get_server_software(provider)

        return name, source

    async def _get_server_software_version(
        self, provider: str
    ) -> tuple[str | None, str]:
        """Queries the server software version.

        Parameters
        ----------
        provider : str
            provider whose property is to be retrieved

        Returns
        -------
        tuple[str | None, str]
            software version (or None on error) and its source
        """

        _name, version, source = await self._get_server_software(provider)

        return version, source

    async def _get_server_software(
        self, provider: str
    ) -> tuple[str | None, str | None, str]:
        """Queries the software name and version of a server.

        Parameters
        ----------
        provider : str
            provider whose property is to be retrieved

        Returns
        -------
        tuple[str | None, str | None, str]
            server software name and version (or None on error) and their source
        """

        # Get the possibly cached server software.
        name = self._server_software[provider][SOFTWARE_INFO_NAME_KEY]
        version = self._server_software[provider][SOFTWARE_INFO_VERSION_KEY]

        # Return the cached server software.
        if name and version:
            return name, version, SOFTWARE_VERSION_URL

        self.register_plugin("xep_0092")

        try:
            software_info = await cast(XEP_0092, self["xep_0092"]).get_version(
                JID(provider), timeout=REQUEST_TIMEOUT_SECONDS
            )

        except IqError as e:
            log.warning(
                "%s: Could not get server software: Type '%s' - Condition '%s': %s",
                provider,
                e.etype,
                e.condition,
                e.text,
            )
            return None, None, SOFTWARE_VERSION_URL

        except IqTimeout:
            log.warning(
                "%s: Could not get server software within %s second/s",
                provider,
                REQUEST_TIMEOUT_SECONDS,
            )
            return None, None, SOFTWARE_VERSION_URL

        name = software_info[SOFTWARE_INFO_KEY][SOFTWARE_INFO_NAME_KEY]
        version = software_info[SOFTWARE_INFO_KEY][SOFTWARE_INFO_VERSION_KEY]

        # Cache the server software for subsequent retrievals.
        # That is done to avoid multiple server requests when multiple properties are
        # retrieved via the same request.
        self._server_software[provider][SOFTWARE_INFO_NAME_KEY] = name
        self._server_software[provider][SOFTWARE_INFO_VERSION_KEY] = version

        return name, version, SOFTWARE_VERSION_URL


class XmppRegistrationClient(ClientXMPP):
    """Checks a provider's registration support.

    Communication flow: XMPP bot <-> provider server
    """

    def __init__(
        self, provider: str, provider_data: dict[str, ProviderDetailsT]
    ) -> None:
        log.info("%s: Starting registration support check", provider)

        self._provider = provider
        self._jid = self._generate_jid()
        self._ibr_provided = False

        ClientXMPP.__init__(self, self._jid, self._generate_password())

        self._provider_data = provider_data

        self.add_event_handler("connection_failed", self._handle_connection_failed)
        self.add_event_handler("failed_auth", self._handle_ibr_not_provided)
        self.add_event_handler("register", self._handle_ibr_provided)

        self.register_plugin("xep_0077")
        cast(XEP_0077, self["xep_0077"]).force_registration = True

        self.connect()

    def _handle_connection_failed(self, error: OSError | str) -> None:
        """Handles the case that the server is not reachable.

        This is needed because connection failures result in unlimited connection
        establishment attempts blocking further checks.
        The original reason for handling connection failures were unavailable DNS
        records.

        Parameters
        ----------
        error : OSError | str
            error in case of an OSError or error message if no DNS records are available
        """

        # Ignore DNS errors for unsupported address families (EAFNOSUPPORT) and if the
        # requested address cannot be assigned (EADDRNOTAVAIL).
        # That is needed because this method is also called if the connection could not
        # be established via a specific IP address family (IPv4/IPv6) or if the address
        # could not be assigned.
        # But a connection failure because of a problem with one address family does not
        # result in a complete connection failure if another address family can be used.
        # The same applies to the case if one address cannot be assigned but another one
        # can be assigned.
        # Only if the server is not reachable at all, the connection establishment
        # should be canceled without further connection attempts.
        if isinstance(error, OSError) and (
            error.errno in (EAFNOSUPPORT, EADDRNOTAVAIL)
        ):
            return

        log.warning(
            "%s: Connection could not be established: %s", self._provider, error
        )
        self._update_property(IBR_KEY, False, IBR_URL)
        self.disconnect()

    def _handle_ibr_not_provided(self, _error_stanza: Failure) -> None:
        """Handles the case that the server did not respond to an In-Band Registration
        request.

        Parameters
        ----------
        _error_stanza : Failure
            stanza from the server in case of a failed authentication
        """

        # Skip further processing if this method is triggered after handling the In-Band
        # Registration response.
        if self._ibr_provided:
            return

        self._update_property(REGISTRATION_WEB_PAGE_KEY, {}, IBR_URL)
        self._update_property(IBR_EMAIL_ADDRESS_REQUIRED_KEY, False, IBR_URL)
        self._update_property(IBR_CAPTCHA_REQUIRED_KEY, False, CAPTCHA_URL)
        self._update_property(IBR_KEY, False, IBR_URL)

    async def _handle_ibr_provided(self, iq: Iq) -> None:
        """Handles an In-Band Registration response from the server to register on it.

        Parameters
        ----------
        iq : Iq
            IQ stanza from the server in response to the In-Band Registration request
        """

        # Skip further processing if this method is triggered after handling the In-Band
        # Registration response.
        if self._ibr_provided:
            return

        self._ibr_provided = True

        log.debug("%s: Checking registration properties", self._provider)

        property_updates: dict[
            str, Callable[[Iq], tuple[Any, str] | Awaitable[tuple[Any, str]]]
        ] = {
            REGISTRATION_WEB_PAGE_KEY: self._get_web_registration_page,
            IBR_EMAIL_ADDRESS_REQUIRED_KEY: self._ibr_email_address_required,
            IBR_CAPTCHA_REQUIRED_KEY: self._ibr_captcha_required,
            IBR_KEY: self._check_in_band_registrations,
        }

        for property_name, check_property in property_updates.items():
            if asyncio.iscoroutinefunction(check_property):
                new_property_content, new_property_source = await check_property(iq)
            else:
                new_property_content, new_property_source = cast(
                    tuple[Any, str], check_property(iq)
                )

            self._update_property(
                property_name, new_property_content, new_property_source
            )

        in_band_registrations_possible = self._provider_data[self._provider][IBR_KEY][
            "content"
        ]

        captcha_required = self._provider_data[self._provider][
            IBR_CAPTCHA_REQUIRED_KEY
        ]["content"]

        if in_band_registrations_possible and not captcha_required:
            self.add_event_handler("stream_negotiated", self._delete_account)
        else:
            self.disconnect()

    def _update_property(
        self, property_name: str, property_content: Any, property_source: str
    ) -> None:
        """Updates a provider property in case of changes.

        Parameters
        ----------
        property_name : str
            property to be checked
        property_content : Any
            value of the property's content field or None on error
        property_source : str
            value of the property's source field
        """

        update_property(
            self._provider,
            self._provider_data[self._provider],
            property_name,
            property_content,
            property_source,
        )

    def _get_web_registration_page(self, iq: Iq) -> tuple[dict[str, str], str]:
        """Retrieves a URL for web registrations from the In-Band Registration response.

        Parameters
        ----------
        iq : Iq
            In-Band Registration response IQ stanza that can contain a web registration
            URL

        Returns
        -------
        tuple[dict[str, str], str]
            language codes mapped to web registration URLs and their source
        """

        log.debug("%s: Checking for web registration URL", self._provider)

        if iq.match("iq/register/oob"):
            url = iq["register"]["oob"]["url"]
            log.debug(
                "%s: Retrieved URL for web registrations: %s", self._provider, url
            )
            return {"en": url}, IBR_URL

        return {}, IBR_URL

    def _ibr_email_address_required(self, iq: Iq) -> tuple[bool, str]:
        """Checks whether an email address is required for in-band registrations.

        Parameters
        ----------
        iq : Iq
            In-Band Registration response IQ stanza that can contain an email address
            field

        Returns
        -------
        tuple[bool, str]
            whether an email address must be provided for in-band registrations amd its
            source
        """

        log.debug(
            "%s: Checking whether email address is required for in-band registrations",
            self._provider,
        )

        if iq.match("iq/register/form"):
            for field in iq["register"]["form"]["fields"]:
                field_key = field[DATA_FORM_FIELD_ATTRIBUTE_KEY]
                if field_key == REGISTRATION_EMAIL_ADDRESS_KEY:
                    required = field.get_required()
                    log.debug(
                        "%s: Email address is %srequired for in-band registrations",
                        self._provider,
                        "" if required else "not ",
                    )
                    return required, IBR_URL

        log.debug(
            "%s: Email address is not required for in-band registrations",
            self._provider,
        )
        return False, IBR_URL

    def _ibr_captcha_required(self, iq: Iq) -> tuple[bool, str]:
        """Checks whether a CAPTCHA is required for in-band registrations.

        Parameters
        ----------
        iq : Iq
            In-Band Registration response IQ stanza that can contain a CAPTCHA

        Returns
        -------
        tuple[bool, str]
            whether a CAPTCHA must be solved for in-band registrations and its source
        """

        log.debug(
            "%s: Checking whether CAPTCHA is required for in-band registrations",
            self._provider,
        )

        if iq.match("iq/register/form"):
            for field in iq["register"]["form"]["fields"]:
                field_key = field[DATA_FORM_FIELD_ATTRIBUTE_KEY]
                if field_key in REGISTRATION_CAPTCHA_KEYS:
                    required = field.get_required()
                    log.debug(
                        "%s: CAPTCHA is %s required for in-band registrations",
                        self._provider,
                        "" if required else "not ",
                    )
                    return required, CAPTCHA_URL

        log.debug(
            "%s: CAPTCHA is not required for in-band registrations", self._provider
        )
        return False, CAPTCHA_URL

    async def _check_in_band_registrations(self, iq: Iq) -> tuple[bool, str]:
        """Checks whether in-band registrations are possible by trying to create an
        account.

        Parameters
        ----------
        iq : Iq
            In-Band Registration response IQ stanza that can contain a data form to be
            completed for registration or regular registration fields

        Returns
        -------
        tuple[bool, str]
            whether the provider supports account creation via In-Band Registration and
            its source
        """

        log.debug("%s: Checking in-band registrations", self._provider)

        if iq.match("iq/register/form"):
            request = self.Iq()
            request["type"] = "set"
            request["register"]["form"].set_type("submit")
            request["register"]["form"]["fields"] = iq["register"]["form"]["fields"]

            for field in request["register"]["form"]["fields"]:
                field_key = field[DATA_FORM_FIELD_ATTRIBUTE_KEY]
                if field_key == REGISTRATION_USERNAME_KEY:
                    field.set_value(self._username)
                if field_key == REGISTRATION_PASSWORD_KEY:
                    field.set_value(self.password)

            return await self._create_account(request), IBR_URL

        if iq.match("iq/register/fields"):
            request = self.Iq()
            request["type"] = "set"
            request["register"][REGISTRATION_USERNAME_KEY] = self._username
            request["register"][REGISTRATION_PASSWORD_KEY] = self.password

            return await self._create_account(request), IBR_URL

        return False, IBR_URL

    async def _create_account(self, iq: Iq) -> bool:
        """Creates an account.

        Parameters
        ----------
        iq : Iq
            In-Band Registration request IQ stanza containing a completed data form to
            be used for registration or regular registration fields

        Returns
        -------
        bool
            whether the account could be created
        """

        try:
            log.debug("%s: Creating account '%s'", self._provider, self._jid)
            await iq.send(timeout=REQUEST_TIMEOUT_SECONDS)

        except IqError as e:
            error_message = e.iq["error"]["text"]
            log.debug(
                "%s: Could not create account '%s': %s",
                self._provider,
                self._jid,
                error_message,
            )

            # The CAPTCHA failure is checked here and not earlier because there are
            # servers sending registration responses even if they do not allow
            # in-band registrations and respond to a completed form with an error
            # afterwards.
            return CAPTCHA_STRING in error_message.lower()

        except IqTimeout:
            log.warning(
                "%s: No response from server while creating account '%s'",
                self._provider,
                self._jid,
            )
            return False

        else:
            log.debug("%s: Account '%s' created", self._provider, self._jid)
            return True

    async def _delete_account(self, _event_data: Any) -> None:
        """Deletes an account.

        Parameters
        ----------
        _event_data : Any
            data provided by the event that triggers this method
        """

        try:
            log.debug("%s: Deleting account %s", self._provider, self._jid)
            await cast(XEP_0077, self["xep_0077"]).cancel_registration(
                timeout=REQUEST_TIMEOUT_SECONDS
            )
            log.debug("%s: Account %s deleted", self._provider, self._jid)

        except IqError as e:
            error_message = e.iq["error"]["text"]
            log.warning(
                "%s: Could not delete account '%s': %s",
                self._provider,
                self._jid,
                error_message,
            )

        except IqTimeout:
            log.warning(
                "%s: No response from server while deleting account %s",
                self._provider,
                self._jid,
            )
            self.disconnect()

    def _generate_jid(self) -> str:
        """Generates a JID with a random local part and sets the username.

        Returns
        -------
        str
            generated JID
        """

        self._username = "".join(
            secrets.choice(RANDOM_USERNAME_CHARACTERS)
            for i in range(RANDOM_USERNAME_LENGTH)
        )
        jid = f"{self._username}@{self._provider}"

        log.debug("JID generated: %s", jid)

        return jid

    def _generate_password(self) -> str:
        """Generates a random password.

        Returns
        -------
        str
            generated password
        """

        password = "".join(
            secrets.choice(RANDOM_PASSWORD_CHARACTERS)
            for i in range(RANDOM_PASSWORD_LENGTH)
        )
        log.debug("Password generated: %s", password)

        return password
