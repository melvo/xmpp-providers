#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
# SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains a bot which runs data gathering tools and creates commits on GitLab."""

import logging

import gitlab

from tools.automation.manager import BotManager
from tools.common import GITLAB_HOST
from tools.common import PROVIDERS_FILE_PATH

log = logging.getLogger()

GITLAB_URL = f"https://{GITLAB_HOST}"
PROJECT_ID = 6857
COMMIT_MESSAGE_PREFIX = "providers: Update"
COMMIT_MESSAGE_SUFFIX_ALL = "all providers"


class GitLabBot:
    """Runs bots for data gathering and manages GitLab merge requests and commits."""

    def __init__(self, branch: str, bot_token: str, jid: str, password: str) -> None:
        """
        Parameters
        ----------
        branch: str
            Git branch to create and push commits to
        bot_token : str
            GitLab bot personal API access token
        jid : str
            bare JID for the XMPP bot
        password : str
            password for the XMPP bot
        """

        log.info("Starting GitLab bot")

        bot_gitlab_session = gitlab.Gitlab(url=GITLAB_URL, private_token=bot_token)
        self._main_project = bot_gitlab_session.projects.get(PROJECT_ID)
        self._branch = branch

        self._checked_providers, self._updated_providers = BotManager(
            jid, password
        ).update_providers()

        if not self._updated_providers:
            log.info("Skipping further processing since no changes have been detected")
            return

        self._commit_changes()

    def _commit_changes(self) -> None:
        """Creates a commit that includes all provider updates."""

        with open(PROVIDERS_FILE_PATH) as providers_file:
            file_data = providers_file.read()

        data = {
            "branch": self._branch,
            "commit_message": self._generate_commit_message(),
            "actions": [
                {
                    "action": "update",
                    "file_path": PROVIDERS_FILE_PATH,
                    "content": file_data,
                }
            ],
        }

        commit = self._main_project.commits.create(data)
        log.info("Commit with title '%s' and ID '%s' created", commit.title, commit.id)

    def _generate_commit_message(self) -> str:
        """Generates a commit message for the updated providers.

        Returns
        -------
        str
            commit message including the updated providers
        """

        message = f"{COMMIT_MESSAGE_PREFIX} "

        if self._checked_providers == self._updated_providers:
            message += COMMIT_MESSAGE_SUFFIX_ALL
        else:
            for i, provider in enumerate(self._updated_providers):
                if i != 0:
                    message += ", "
                message += provider

        return message
