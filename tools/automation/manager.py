#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
# SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains a manager for all bots updating the providers file."""

from typing import cast

import logging
from datetime import datetime
from datetime import UTC

from tools.automation.common import update_providers_file
from tools.automation.web import WebBot
from tools.automation.xmpp import XmppBot
from tools.common import load_json_file
from tools.common import PROVIDERS_FILE_PATH
from tools.data_types import ProviderDetailsT

log = logging.getLogger()


class BotManager:
    """Manages the bots.

    Parameters
    ----------
    jid : str
        bare JID for the XMPP bot
    password : str
        password for the XMPP bot
    """

    def __init__(self, jid: str, password: str) -> None:
        self._jid = jid
        self._password = password

    def update_providers(self) -> tuple[list[str], list[str]]:
        """Runs all bots, determines which providers are updated and writes the changes
        including the current date to the providers file.

        Returns
        -------
        tuple[list[str], list[str]]
            checked and updated providers
        """

        updated_providers: list[str] = []

        old_json_data = load_json_file(PROVIDERS_FILE_PATH)

        if old_json_data is None:
            return [], updated_providers

        old_provider_data = cast(dict[str, ProviderDetailsT], old_json_data)
        checked_providers = list(old_provider_data.keys())

        self._run_bots()

        new_json_data = load_json_file(PROVIDERS_FILE_PATH)

        if new_json_data is None:
            return checked_providers, updated_providers

        new_provider_data = cast(dict[str, ProviderDetailsT], new_json_data)

        for provider, old_properties in old_provider_data.items():
            new_properties = new_provider_data[provider]
            if old_properties != new_properties:
                updated_providers.append(provider)
                new_properties["latestChange"]["content"] = datetime.now(UTC).strftime(
                    "%Y-%m-%d"
                )

        update_providers_file(new_provider_data)

        return checked_providers, updated_providers

    def _run_bots(self) -> None:
        """Runs all bots updating the providers file."""

        WebBot()
        XmppBot(self._jid, self._password)
