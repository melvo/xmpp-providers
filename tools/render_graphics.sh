#!/bin/bash -e

# SPDX-FileCopyrightText: 2024 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# This script renders various graphics.
# It takes SVG files and produces PNG files.

REPOSITORY_DIRECTORY=$(dirname "$(readlink -f "${0}")")/..
OUTPUT_DIRECTORY="${REPOSITORY_DIRECTORY}/rendered-graphics"

render_gitlab_project_avatar() {
    render_svg logo.svg gitlab-project-avatar.png 400 400
}

render_gitlab_bot_avatar() {
    render_svg_with_margin_and_background logo.svg gitlab-bot-avatar.png 400 400 35 "#dee2e6"
}

render_group_chat_avatar() {
    render_svg_with_margin_and_background logo.svg group-chat-avatar.png 400 400 35 "#dee2e6"
}

render_mastodon_avatar() {
    render_svg_with_margin_and_background logo.svg mastodon-avatar.png 400 400 35 "#dee2e6"
}

# $1: relative input file path
# $2: output filename
# $3: width
# $4: height
# $5: margin
# $6: background color
render_svg_with_margin_and_background() {
    input_file_path="${REPOSITORY_DIRECTORY}/${1}"
    temporary_file_name=$(basename "${1}")
    temporary_file_path="${OUTPUT_DIRECTORY}/tmp-${temporary_file_name}"
    output_file_path="${OUTPUT_DIRECTORY}/${2}"
    inkscape -o "${temporary_file_path}" -w "${3}" -h "${4}" --export-margin="${5}" "${input_file_path}"
    inkscape -o "${output_file_path}" -w "${3}" -h "${4}" --export-background="${6}" "${temporary_file_path}"
    rm "${temporary_file_path}"
    optimize_png "${output_file_path}"
    echo "Created ${output_file_path}"
}

# $1: relative input file path
# $2: output filename
# $3: width
# $4: height
render_svg() {
    input_file_path="${REPOSITORY_DIRECTORY}/${1}"
    output_file_path="${OUTPUT_DIRECTORY}/${2}"
    inkscape -o "${output_file_path}" -w "${3}" -h "${4}" "${input_file_path}"
    optimize_png "${output_file_path}"
    echo "Created ${output_file_path}"
}

# $1: PNG file to be optimized
optimize_png() {
    optipng -o7 -quiet "${1}" >/dev/null && \
    advpng -z4 "${1}" >/dev/null
}

mkdir -p "${OUTPUT_DIRECTORY}"
render_gitlab_project_avatar
render_gitlab_bot_avatar
render_group_chat_avatar
render_mastodon_avatar
