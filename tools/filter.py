#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
# SPDX-FileCopyrightText: 2021 Michel Le Bihan <michel@lebihan.pl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This script filters the providers and extracts those matching specific criteria."""

from __future__ import annotations

from typing import Any
from typing import cast

import logging
from collections import defaultdict
from collections import OrderedDict
from pathlib import Path

from tools.common import Category
from tools.common import convert_python_data_to_json_string
from tools.common import create_parent_directories
from tools.common import load_json_file
from tools.common import PROVIDERS_FILE_PATH
from tools.criteria import CRITERIA_A
from tools.criteria import CRITERIA_B
from tools.criteria import CRITERIA_C
from tools.criteria import CRITERIA_D
from tools.data_types import AllProviderDetailsT
from tools.data_types import FilteredDetailsT
from tools.data_types import ProviderDetailsT

log = logging.getLogger()

CRITERIA_DOCUMENTATION = "https://invent.kde.org/melvo/xmpp-providers#criteria"

DOMAINS_ONLY_FILE_NAME_SUFFIX = "s"
RESULT_FILE_PATH = "results/%s.json"
PROVIDER_LIST_FILE_PATH = "%s-%s%s%s"
PROPERTY_CONTENT_KEY = "content"


class Filter:
    """Filters the providers and extracts those matching specific criteria.

    Parameters
    ----------
    providers : list[str]
        domains of providers to be filtered or an empty list for filtering all providers
    input_category : Category
        category used for filtering (all categories are used if Category.All is passed)
    domains_only : bool
        whether to output a list of provider domains instead of the properties
    categories_included : bool
        whether to add the category of each provider to its entry (implicitly false if
        domains_only is true)
    result_files : bool
        whether to create additional files each containing the result of one provider
    """

    def __init__(
        self,
        providers: list[str],
        input_category: Category,
        domains_only: bool,
        categories_included: bool,
        result_files: bool,
    ) -> None:
        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        provider_data = cast(dict[str, ProviderDetailsT], json_data)

        category = cast(Category, input_category)
        categories = list[Category]

        if category == Category.ALL:
            categories = [
                Category.AUTOMATICALLY_CHOSEN,
                Category.MANUALLY_SELECTABLE,
                Category.COMPLETELY_CUSTOMIZABLE,
                Category.AUTOCOMPLETE,
            ]
        else:
            categories = [category]

        results = defaultdict(lambda: defaultdict(lambda: defaultdict(str)))

        for category in categories:
            self._create_provider_list(
                provider_data,
                providers,
                category,
                domains_only,
                categories_included,
                results,
            )

        if result_files:
            self._create_result_files(results)

    def _create_provider_list(
        self,
        provider_data: dict[str, ProviderDetailsT],
        providers: list[str],
        category: Category,
        domains_only: bool,
        categories_included: bool,
        results: dict[str, Any],
    ) -> None:
        """Creates a file for providers of a specific category.

        Parameters
        ----------
        provider_data : dict[str, ProviderDetailsT]
            provider data to be filtered
        providers : list[str]
            domains of providers to be filtered or an empty list for filtering all
            providers
        category : Category
            category used for filtering
        domains_only : bool
            whether to output a list of provider domains instead of the properties
        categories_included : bool
            whether to add the category of each provider to its entry (implicitly false
            if domains_only is true)
        results : dict[str, Any]
            results of the filtering
        """

        log.debug("STARTING creation of provider list for category %s", category.value)

        provider_count = len(providers)
        extracted_providers = []

        for jid, properties in provider_data.items():
            if provider_count == 0 or jid in providers:
                log.debug("  %s: Filtering", jid)

                output_properties = self._create_output_properties(properties.copy())
                assert isinstance(output_properties, OrderedDict)
                provider_in_category = False

                if categories_included:
                    included_categories = [
                        Category.AUTOMATICALLY_CHOSEN,
                    ]

                    if category != Category.AUTOMATICALLY_CHOSEN:
                        included_categories.append(Category.MANUALLY_SELECTABLE)

                        if category != Category.MANUALLY_SELECTABLE:
                            included_categories.append(Category.COMPLETELY_CUSTOMIZABLE)

                            if category != Category.COMPLETELY_CUSTOMIZABLE:
                                included_categories.append(Category.AUTOCOMPLETE)

                    for included_category in included_categories:
                        if self._filter_provider(
                            included_category,
                            output_properties,  # pyright: ignore [reportArgumentType]
                            results[jid][included_category.value],
                        ):
                            output_properties["category"] = included_category.value
                            output_properties.move_to_end("category", False)
                            provider_in_category = True
                            break
                elif self._filter_provider(
                    category,
                    output_properties,  # pyright: ignore [reportArgumentType]
                    results[jid][category.value],
                ):
                    provider_in_category = True

                if provider_in_category:
                    output_properties["jid"] = jid
                    output_properties.move_to_end("jid", False)

                    extracted_providers.append(output_properties)

        if provider_count == 0:
            provider_count = len(provider_data)

        log.debug(
            "RESULT: %s of %s providers in category %s",
            len(extracted_providers),
            provider_count,
            category.value,
        )
        log.debug(
            "The criteria are specified in the README: %s", CRITERIA_DOCUMENTATION
        )

        if domains_only:
            extracted_providers = [provider["jid"] for provider in extracted_providers]

        formatted_json_string = convert_python_data_to_json_string(extracted_providers)
        providers_file_path = Path(PROVIDERS_FILE_PATH)

        provider_list_file_path = PROVIDER_LIST_FILE_PATH % (
            f"{providers_file_path.parent}/{providers_file_path.stem}",
            category.value,
            DOMAINS_ONLY_FILE_NAME_SUFFIX if domains_only else "",
            providers_file_path.suffix,
        )

        with open(provider_list_file_path, "w") as provider_list_file:
            provider_list_file.write(formatted_json_string)
            log.info("'%s' created", provider_list_file_path)

    @staticmethod
    def _create_output_properties(properties: ProviderDetailsT) -> FilteredDetailsT:
        """Creates the output properties of a provider consisting only of the
        information relevant to the users.

        Parameters
        ----------
        properties : dict
            properties of the provider

        Returns
        -------
        dict
            only consisting of relevant properties
        """

        new_properties = cast(FilteredDetailsT, OrderedDict())

        for property_name, property_data in properties.items():
            property_data = cast(AllProviderDetailsT, property_data)
            new_properties[property_name] = property_data[PROPERTY_CONTENT_KEY]

        return new_properties

    def _filter_provider(
        self, category: Category, properties: FilteredDetailsT, results: dict[str, Any]
    ) -> bool:
        """Filters properties by a passed category.

        Parameters
        ----------
        category : Category
            category used for filtering
        properties : dict
            properties of the provider
        results : dict
            results of the filtering

        Returns
        -------
        bool
            whether the provider belongs to the category
        """

        if category == Category.AUTOCOMPLETE:
            return self._check_properties(category, properties, CRITERIA_D, results)

        if category == Category.COMPLETELY_CUSTOMIZABLE:
            return self._check_properties(category, properties, CRITERIA_C, results)

        if category == Category.MANUALLY_SELECTABLE:
            return self._check_properties(category, properties, CRITERIA_B, results)

        if category == Category.AUTOMATICALLY_CHOSEN:
            return self._check_properties(category, properties, CRITERIA_A, results)

        return False

    @staticmethod
    def _check_properties(
        category: Category,
        properties: FilteredDetailsT,
        criteria: dict[str, Any],
        results: dict[str, Any],
    ) -> bool:
        """Checks if properties meet specific criteria.

        Parameters
        ----------
        category : Category
            category used for filtering
        properties : dict
            properties of the provider
        criteria : dict
            criteria that are checked
        results : dict
            results of the check

        Returns
        -------
        bool
            whether all properties meet the criteria
        """

        check_succeeded = True

        for property_string, criterion in criteria.items():
            property_names = property_string.split(",")
            property_values = [
                properties[property_name] for property_name in property_names
            ]

            if not criterion(*property_values):
                for property_name in property_names:
                    property_value = properties[property_name]
                    results[property_name] = property_value
                    log.debug(
                        "    %s: %s not meeting the criterion for category %s",
                        property_name,
                        property_value,
                        category.value,
                    )

                if check_succeeded:
                    check_succeeded = False

        return check_succeeded

    @staticmethod
    def _create_result_files(results: dict[str, Any]) -> None:
        """Creates files for the results of the filtering.

        Parameters
        ----------
        results: dict
            results of the filtering
        """

        create_parent_directories(Path(RESULT_FILE_PATH))

        for jid, result in results.items():
            formatted_json_string = convert_python_data_to_json_string(result)

            result_file_path = RESULT_FILE_PATH % jid
            with open(result_file_path, "w") as result_file:
                result_file.write(formatted_json_string)
                log.info("'%s' created", result_file_path)
