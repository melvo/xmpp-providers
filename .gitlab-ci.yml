# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: CC0-1.0

workflow:
  rules:
    - if: $CI_COMMIT_REF_PROTECTED == "true"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

reuse:
  stage: test
  interruptible: true
  dependencies: []
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  image:
    name: fsfe/reuse:4.0.3
    entrypoint: [""]
  script: reuse lint

markdown:
  stage: test
  image:
    name: davidanson/markdownlint-cli2:v0.14.0
    entrypoint: [""]
  script: markdownlint-cli2 "**/*.md"

urls:
  stage: test
  interruptible: true
  dependencies: []
  image: python:3.12-slim
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  before_script:
    - apt-get update -y && apt-get install -y --no-install-recommends git
  script:
    - pip install -r requirements-check-urls.txt
    - git fetch origin master
    - git remote rm fork || true
    - git remote add fork $CI_MERGE_REQUEST_SOURCE_PROJECT_URL.git
    - git fetch fork $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    - git diff --name-only origin/master fork/$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME | xargs python3 -m tools.run check_urls

urls-scheduled:
  stage: test
  interruptible: true
  allow_failure: true
  dependencies: []
  image: python:3.12-slim
  rules:
    # SCHEDULED_URL_CHECK is set in GitLab's pipeline schedules section
    - if: $SCHEDULED_URL_CHECK == "true"
    - when: never
  script:
    - pip install -r requirements-check-urls.txt
    - python3 -m tools.run check_urls

json:
  stage: test
  interruptible: true
  dependencies: []
  image: python:3.12-slim
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - "**/*.json"
  script:
    - python3 -m tools.run prettify -e

python:
  stage: test
  interruptible: true
  dependencies: []
  # https://hub.docker.com/r/nikolaik/python-nodejs
  image: nikolaik/python-nodejs:python3.12-nodejs22-slim
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - "**/*.py"
  before_script:
    - apt-get update -y && apt-get install -y --no-install-recommends git
    - npm install -g pyright
    # Pyright needs all dependencies installed
    - pip install -r requirements-python.txt
  script:
    - isort --check-only .
    - black --check .
    - ruff check .
    - pyright .

web-bot-scheduled:
  stage: build
  interruptible: true
  allow_failure: true
  dependencies: []
  image: python:3.12-slim
  rules:
    # SCHEDULED_WEB_BOT is set in GitLab's pipeline schedules section
    - if: $SCHEDULED_WEB_BOT == "true"
    - when: never
  script:
    - pip install -r requirements-web-bot.txt
    - python3 -m tools.run web_bot -u

gitlab-bot-scheduled:
  stage: build
  allow_failure: true
  dependencies: []
  image: python:3.12-slim
  rules:
    # SCHEDULED_GITLAB_BOT is set in GitLab's pipeline schedules section
    - if: $SCHEDULED_GITLAB_BOT == "true"
    - when: never
  script:
    - pip install -r requirements-gitlab-bot.txt
    - python3 -m tools.run $BASE_TOOL_OPTIONS gitlab_bot $CI_COMMIT_BRANCH $GITLAB_BOT_TOKEN $XMPP_BOT_JID $XMPP_BOT_PASSWORD

original-files:
  stage: deploy
  image: busybox:latest
  rules:
    - if: $CI_COMMIT_REF_PROTECTED == "true"
  script:
    - echo "Providing original files"
  artifacts:
    paths:
      - "providers.json"
      - "clients.json"

filtered-provider-lists:
  stage: deploy
  image: python:3.12-slim
  rules:
    - if: $CI_COMMIT_REF_PROTECTED == "true"
  script:
    - python3 -m tools.run -d filter -c -r
    - python3 -m tools.run filter -s
  artifacts:
    paths:
      - "providers-*.json"
      - "results/*.json"

badges:
  stage: deploy
  image: python:3.12-slim
  rules:
    - if: $CI_COMMIT_REF_PROTECTED == "true"
  needs:
    - job: filtered-provider-lists
  script:
    - python3 -m tools.run -d badge -c
  artifacts:
    paths:
      - "count-badge-*.svg"
      - "badges/*.svg"
